pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}

rootProject.name = "Fermion"

sequenceOf("core-api" to "api", "core-impl" to "core-default-implementation", "vmtype" to "vm-types").forEach {
    include(it.first)
    project(":${it.first}").name = it.second
}
