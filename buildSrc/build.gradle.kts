plugins {
    `kotlin-dsl`
    `maven-publish`
}

repositories {
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    // Licenser
    implementation(group = "gradle.plugin.net.minecrell", name = "licenser", version = "0.4.1")

    // Versions
    implementation(group = "com.github.ben-manes", name = "gradle-versions-plugin", version = "0.38.0")
}
