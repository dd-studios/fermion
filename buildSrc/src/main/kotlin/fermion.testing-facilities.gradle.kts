import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.withType

plugins {
    id("fermion.java-conventions")
}

dependencies {
    /* ============================ TESTING-ONLY LIBRARIES ============================ */
    // AssertJ Fluent Assertions
    testImplementation(group = "org.assertj", name = "assertj-core", version = "3.19.0")

    // JUnit5 Jupiter
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter", version = "5.8.0-M1")

    /* ============================ TESTING-ONLY RUNTIME LIBRARIES ============================ */
}

tasks.withType<Test>().configureEach {
    this.useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
