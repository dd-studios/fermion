import org.gradle.kotlin.dsl.dependencies
import java.util.Calendar

plugins {
    `java-library`
    `maven-publish`
    id("net.minecrell.licenser")
    id("com.github.ben-manes.versions")
}

java {
    sourceCompatibility = JavaVersion.VERSION_16
    targetCompatibility = JavaVersion.VERSION_16
}

license {
    header = project.file("NOTICE").let { if (it.exists()) it else project.rootProject.file("NOTICE") }
    include("**/*.java")

    ext["year"] = Calendar.getInstance().get(Calendar.YEAR)
    ext["name"] = "Dwarved Donuts Studios"
    ext["email"] = "<EMAIL>"
    ext["app"] = "Fermion"

    ignoreFailures = false
}

repositories {
    mavenCentral()
}

dependencies {
    /* ============================ COMPILE TIME ONLY ============================ */
    // JetBrains Java Annotations
    implementation(group = "org.jetbrains", name = "annotations", version = "20.1.0")

    /* ============================ RUNTIME LIBRARIES ============================ */
}
