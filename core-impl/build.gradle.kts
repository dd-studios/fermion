plugins {
    id("fermion.java-conventions")
    id("fermion.testing-facilities")
}

dependencies {
    val ow2Version: String by project

    implementation(project(":api"))
    implementation(group = "org.ow2.asm", name = "asm", version = ow2Version)
    implementation(group = "org.ow2.asm", name = "asm-tree", version = ow2Version)
    implementation(group = "org.ow2.asm", name = "asm-util", version = ow2Version)
}
