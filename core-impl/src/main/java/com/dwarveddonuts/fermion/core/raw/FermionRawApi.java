package com.dwarveddonuts.fermion.core.raw;

import com.dwarveddonuts.fermion.api.raw.util.RawApi;

public final class FermionRawApi implements RawApi {
    private static final class SingletonHelper {
        private static final RawApi INSTANCE = new FermionRawApi();
    }

    public static RawApi get() {
        return SingletonHelper.INSTANCE;
    }
}
