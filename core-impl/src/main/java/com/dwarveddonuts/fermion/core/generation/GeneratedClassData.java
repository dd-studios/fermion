package com.dwarveddonuts.fermion.core.generation;

import com.dwarveddonuts.fermion.api.description.ClassDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.ClassPrototype;

record GeneratedClassData(ClassPrototype prototype, ClassDescriptor descriptor) {
    static String uniqueKey(final ClassDescriptor descriptor) {
        return descriptor.getClassName();
    }

    static GeneratedClassData of(final ClassPrototype prototype, final ClassDescriptor descriptor) {
        return new GeneratedClassData(prototype, descriptor);
    }
}
