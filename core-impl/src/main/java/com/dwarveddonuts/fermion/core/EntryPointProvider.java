package com.dwarveddonuts.fermion.core;

import com.dwarveddonuts.fermion.FermionParameters;
import com.dwarveddonuts.fermion.api.EntryPoint;
import com.dwarveddonuts.fermion.spi.EntryPointService;

public final class EntryPointProvider implements EntryPointService {
    private EntryPoint entryPoint;
    private FermionParameters parameters;

    @Override
    public void set(final FermionParameters parameters) {
        if (this.parameters == null) {
            synchronized (this) {
                if (this.parameters == null) {
                    this.parameters = parameters;
                }
            }
        }
    }

    @Override
    public EntryPoint x() {
        if (this.entryPoint == null) {
            synchronized (this) {
                if (this.entryPoint == null) {
                    this.entryPoint = new FermionEntryPoint(this.parameters);
                }
            }
        }
        return this.entryPoint;
    }
}
