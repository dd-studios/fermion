package com.dwarveddonuts.fermion.core.generation;

import com.dwarveddonuts.fermion.api.description.MethodDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.MethodPrototype;
import com.dwarveddonuts.fermion.api.flow.generation.MethodGenerationFlow;

import java.util.function.BiConsumer;

record GeneratedMethodData(MethodPrototype prototype, MethodDescriptor descriptor,
                           BiConsumer<MethodDescriptor, MethodGenerationFlow> generator) {
    static String uniqueKey(final MethodDescriptor descriptor) {
        return descriptor.getMethodName() + descriptor.getErasedBytecodeDescriptor();
    }

    static GeneratedMethodData of(final MethodPrototype prototype, final MethodDescriptor descriptor) {
        return new GeneratedMethodData(prototype, descriptor, null);
    }

    boolean hasGenerator() {
        return this.generator != null;
    }

    GeneratedMethodData withGenerator(final BiConsumer<MethodDescriptor, MethodGenerationFlow> generator) {
        if (this.hasGenerator()) throw new IllegalStateException("Already with generator");
        return new GeneratedMethodData(this.prototype, this.descriptor, generator);
    }
}
