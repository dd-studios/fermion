package com.dwarveddonuts.fermion.core.generation;

import com.dwarveddonuts.fermion.api.description.FieldDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.FieldPrototype;
import com.dwarveddonuts.fermion.api.flow.generation.FieldGenerationFlow;

import java.util.function.BiConsumer;

record GeneratedFieldData(FieldPrototype prototype, FieldDescriptor descriptor,
                                 BiConsumer<FieldDescriptor, FieldGenerationFlow> generator) {
    static String uniqueKey(final FieldDescriptor descriptor) {
        return descriptor.getFieldName() + descriptor.getErasedBytecodeName();
    }

    static GeneratedFieldData of(final FieldPrototype prototype, final FieldDescriptor descriptor) {
        return new GeneratedFieldData(prototype, descriptor, null);
    }

    boolean hasGenerator() {
        return this.generator != null;
    }

    GeneratedFieldData withGenerator(final BiConsumer<FieldDescriptor, FieldGenerationFlow> generator) {
        if (this.hasGenerator()) throw new IllegalStateException("Already with generator");
        return new GeneratedFieldData(this.prototype, this.descriptor, generator);
    }
}
