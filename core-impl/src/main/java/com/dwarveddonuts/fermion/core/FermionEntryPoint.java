package com.dwarveddonuts.fermion.core;

import com.dwarveddonuts.fermion.FermionParameters;
import com.dwarveddonuts.fermion.api.EntryPoint;
import com.dwarveddonuts.fermion.api.flow.generation.GenerationFlow;
import com.dwarveddonuts.fermion.api.raw.util.RawApi;
import com.dwarveddonuts.fermion.core.generation.FermionGenerationFlow;
import com.dwarveddonuts.fermion.core.raw.FermionRawApi;

import java.util.function.Consumer;

public final class FermionEntryPoint implements EntryPoint {
    private final FermionParameters parameters;

    FermionEntryPoint(final FermionParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public void generate(final Consumer<GenerationFlow> flow) {
        flow.accept(FermionGenerationFlow.of(this.parameters));
    }

    @Override
    public RawApi raw() {
        return FermionRawApi.get();
    }
}
