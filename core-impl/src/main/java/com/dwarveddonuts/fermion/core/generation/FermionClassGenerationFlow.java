package com.dwarveddonuts.fermion.core.generation;

import com.dwarveddonuts.fermion.FermionParameters;
import com.dwarveddonuts.fermion.api.description.FieldDescriptor;
import com.dwarveddonuts.fermion.api.description.InterfaceDescriptor;
import com.dwarveddonuts.fermion.api.description.MethodDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.FieldPrototype;
import com.dwarveddonuts.fermion.api.description.prototype.MethodPrototype;
import com.dwarveddonuts.fermion.api.flow.ClassResult;
import com.dwarveddonuts.fermion.api.flow.generation.AnnotationGenerationFlow;
import com.dwarveddonuts.fermion.api.flow.generation.ClassGenerationFlow;
import com.dwarveddonuts.fermion.api.flow.generation.FieldGenerationFlow;
import com.dwarveddonuts.fermion.api.flow.generation.MethodGenerationFlow;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

final class FermionClassGenerationFlow implements ClassGenerationFlow {
    private final FermionParameters parameters;
    private final GeneratedClassData classData;
    private final Map<String, Consumer<AnnotationGenerationFlow.Usage>> annotations;
    private final Map<String, GeneratedMethodData> methods;
    private final Map<String, GeneratedFieldData> fields;

    FermionClassGenerationFlow(final FermionParameters parameters, final GeneratedClassData classData) {
        this.parameters = parameters;
        this.classData = classData;
        this.annotations = new HashMap<>();
        this.methods = new HashMap<>();
        this.fields = new HashMap<>();
    }

    @Override
    public void annotate(final InterfaceDescriptor annotation, final Consumer<AnnotationGenerationFlow.Usage> flow) {
        final var annotationName = annotation.getBytecodeName(); // Not using erased since annotations can't be generic
        if (this.annotations.containsKey(annotationName)) {
            throw new IllegalArgumentException("The current class was already annotated with '" + annotation + "'");
        }
        if (annotation.isGenericType()) {
            throw new IllegalArgumentException("Unable to use a generic type as an annotation");
        }
        this.annotations.put(annotationName, flow);
    }

    @Override
    public MethodDescriptor prototypeMethod(final MethodPrototype prototype) {
        // TODO("Additional checks")
        final var descriptor = prototype.getDescriptor();
        // TODO("Allow changing of the above target via services")
        final var methodKey = GeneratedMethodData.uniqueKey(descriptor);
        if (this.methods.containsKey(methodKey)) {
            throw new IllegalArgumentException("Method '" + methodKey + "' was already prototyped");
        }
        this.methods.put(methodKey, GeneratedMethodData.of(prototype, descriptor));
        return descriptor;
    }

    @Override
    public MethodDescriptor generateMethod(final MethodDescriptor descriptor, final BiConsumer<MethodDescriptor, MethodGenerationFlow> flow) {
        final var methodKey = GeneratedMethodData.uniqueKey(descriptor);
        if (!this.methods.containsKey(methodKey)) {
            throw new IllegalArgumentException("Unable to generate method that hasn't been prototyped");
        }
        final var data = this.methods.get(methodKey);
        if (data.hasGenerator()) {
            throw new IllegalArgumentException("Method '" + descriptor + "' was already generated");
        }
        this.methods.put(methodKey, data.withGenerator(flow));
        return descriptor;
    }

    @Override
    public FieldDescriptor prototypeField(final FieldPrototype prototype) {
        // TODO("Additional checks")
        final var descriptor = prototype.getDescriptor();
        // TODO("Allow changing of the above target via services")
        final var fieldKey = GeneratedFieldData.uniqueKey(descriptor);
        if (this.fields.containsKey(fieldKey)) {
            throw new IllegalArgumentException("Field '" + fieldKey + "' was already prototyped");
        }
        this.fields.put(fieldKey, GeneratedFieldData.of(prototype, descriptor));
        return descriptor;
    }

    @Override
    public FieldDescriptor generateField(final FieldDescriptor descriptor, final BiConsumer<FieldDescriptor, FieldGenerationFlow> flow) {
        final var fieldKey = GeneratedFieldData.uniqueKey(descriptor);
        if (!this.fields.containsKey(fieldKey)) {
            throw new IllegalArgumentException("Unable to generate field that hasn't been prototyped");
        }
        final var data = this.fields.get(fieldKey);
        if (data.hasGenerator()) {
            throw new IllegalArgumentException("Field '" + descriptor + "' was already generated");
        }
        this.fields.put(fieldKey, data.withGenerator(flow));
        return descriptor;
    }

    @Override
    public ClassResult end() {
        // TODO("Generate class with ASM")
        return null;
    }
}
