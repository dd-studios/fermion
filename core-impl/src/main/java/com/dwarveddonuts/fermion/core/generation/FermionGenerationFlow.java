package com.dwarveddonuts.fermion.core.generation;

import com.dwarveddonuts.fermion.FermionParameters;
import com.dwarveddonuts.fermion.api.description.ClassDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.ClassPrototype;
import com.dwarveddonuts.fermion.api.flow.ClassResult;
import com.dwarveddonuts.fermion.api.flow.generation.ClassGenerationFlow;
import com.dwarveddonuts.fermion.api.flow.generation.GenerationFlow;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public final class FermionGenerationFlow implements GenerationFlow {
    private final FermionParameters parameters;
    private final Map<String, GeneratedClassData> prototypes;
    private final Set<ClassDescriptor> generated;

    private FermionGenerationFlow(final FermionParameters parameters) {
        this.parameters = parameters;
        this.prototypes = new HashMap<>();
        this.generated = new HashSet<>();
    }

    public static GenerationFlow of(final FermionParameters parameters) {
        return new FermionGenerationFlow(parameters);
    }

    @Override
    public ClassDescriptor prototypeClass(final ClassPrototype prototype) {
        final var target = prototype.getName();
        // TODO("Allow changing of the above target via services")
        if (target.isPrimitive()) {
            throw new IllegalArgumentException("Cannot prototype a primitive: they don't have classes");
        }
        if (target.isArray()) {
            throw new IllegalArgumentException("Cannot prototype an array type: they are auto-generated");
        }
        final String classKey = GeneratedClassData.uniqueKey(target);
        if (this.prototypes.containsKey(classKey)) {
            throw new IllegalArgumentException("Class '" + target + "' was already prototyped");
        }
        this.prototypes.put(classKey, GeneratedClassData.of(prototype, target));
        return target;
    }

    @Override
    public ClassResult generateClass(final ClassDescriptor descriptor, final BiConsumer<ClassDescriptor, ClassGenerationFlow> flow) {
        if (this.generated.contains(descriptor)) {
            throw new IllegalArgumentException("Class '" + descriptor + "' was already generated");
        }

        final String classKey = GeneratedClassData.uniqueKey(descriptor);
        if (!this.prototypes.containsKey(classKey)) {
            throw new IllegalArgumentException("Unable to generate class that has not been prototyped");
        }
        this.generated.add(descriptor);

        final GeneratedClassData classData = this.prototypes.get(classKey);
        final ClassGenerationFlow classFlow = new FermionClassGenerationFlow(this.parameters, classData);
        flow.accept(descriptor, classFlow);
        return classFlow.end();
    }
}
