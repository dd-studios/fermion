module com.dwarveddonuts.fermion.core {
    requires com.dwarveddonuts.vmtypes;
    requires com.dwarveddonuts.fermion.api;
    requires org.jetbrains.annotations;
}
