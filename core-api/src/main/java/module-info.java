module com.dwarveddonuts.fermion.api {
    requires com.dwarveddonuts.vmtypes;
    requires org.jetbrains.annotations;

    exports com.dwarveddonuts.fermion;

    exports com.dwarveddonuts.fermion.api;
    exports com.dwarveddonuts.fermion.api.description;
    exports com.dwarveddonuts.fermion.api.description.prototype;
    exports com.dwarveddonuts.fermion.api.flow;
    exports com.dwarveddonuts.fermion.api.flow.bytecode;
    exports com.dwarveddonuts.fermion.api.flow.bytecode.type;
    exports com.dwarveddonuts.fermion.api.flow.code;
    exports com.dwarveddonuts.fermion.api.flow.generation;
    exports com.dwarveddonuts.fermion.api.lens;
    exports com.dwarveddonuts.fermion.api.raw;
    exports com.dwarveddonuts.fermion.api.raw.annotation;
    exports com.dwarveddonuts.fermion.api.raw.annotation.type;
    exports com.dwarveddonuts.fermion.api.raw.attribute;
    exports com.dwarveddonuts.fermion.api.raw.code;
    exports com.dwarveddonuts.fermion.api.raw.frame;
    exports com.dwarveddonuts.fermion.api.raw.pool;
    exports com.dwarveddonuts.fermion.api.raw.verification;
    exports com.dwarveddonuts.fermion.api.raw.util;

    exports com.dwarveddonuts.fermion.spi;

    uses com.dwarveddonuts.fermion.spi.EntryPointService;
}
