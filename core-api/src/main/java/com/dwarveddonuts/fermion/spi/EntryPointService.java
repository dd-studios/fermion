package com.dwarveddonuts.fermion.spi;

import com.dwarveddonuts.fermion.FermionParameters;
import com.dwarveddonuts.fermion.api.EntryPoint;

public interface EntryPointService {
    void set(final FermionParameters parameters);
    EntryPoint x();
}
