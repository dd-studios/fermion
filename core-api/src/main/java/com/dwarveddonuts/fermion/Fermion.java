package com.dwarveddonuts.fermion;

import com.dwarveddonuts.fermion.api.EntryPoint;
import com.dwarveddonuts.fermion.spi.EntryPointService;

import java.util.ArrayList;
import java.util.Objects;
import java.util.ServiceLoader;

public final class Fermion {
    public static final String APP_NAME = "Fermion";

    private static EntryPointService loadedEntryPoint;

    public static FermionParameters.Builder parameters() {
        return FermionParameters.create();
    }

    public static void bootstrap(final FermionParameters params) {
        if (loadedEntryPoint != null) {
            throw new IllegalStateException("Fermion bootstrap has already been completed");
        }

        final var serviceLoader = ServiceLoader.load(EntryPointService.class);
        final var foundServices = new ArrayList<EntryPointService>();
        serviceLoader.forEach(foundServices::add);

        if (foundServices.isEmpty()) {
            // TODO: Replace with no-op API
            throw new IllegalStateException();
        }

        loadedEntryPoint = foundServices.stream()
                .filter(it -> it.getClass().getName().startsWith(Fermion.class.getPackage().getName()))
                .findFirst()
                .orElseGet(() -> foundServices.get(0));

        Objects.requireNonNull(loadedEntryPoint, "An unknown error has occurred while bootstrapping");
        loadedEntryPoint.set(params);
    }

    public static EntryPoint x() {
        return Objects.requireNonNull(loadedEntryPoint, "No entry point loaded; ensure bootstrap has run").x();
    }
}
