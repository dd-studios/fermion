package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.annotation.ElementValue;

public interface AnnotationDefaultAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "AnnotationDefault";

    ElementValue getDefaultValue();

    void setDefaultValue(final ElementValue defaultValue);
}
