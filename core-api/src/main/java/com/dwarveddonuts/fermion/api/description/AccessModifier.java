package com.dwarveddonuts.fermion.api.description;

public enum AccessModifier {
    DEFAULT,
    PRIVATE,
    PROTECTED,
    PUBLIC
}
