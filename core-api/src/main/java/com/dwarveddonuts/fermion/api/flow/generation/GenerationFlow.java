package com.dwarveddonuts.fermion.api.flow.generation;

import com.dwarveddonuts.fermion.api.description.ClassDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.ClassPrototype;
import com.dwarveddonuts.fermion.api.flow.ClassResult;

import java.util.function.BiConsumer;

public interface GenerationFlow {
    ClassDescriptor prototypeClass(final ClassPrototype prototype);
    ClassResult generateClass(final ClassDescriptor descriptor,
                              final BiConsumer<ClassDescriptor, ClassGenerationFlow> flow);

    default ClassResult generateClass(final ClassPrototype prototype,
                                      final BiConsumer<ClassDescriptor, ClassGenerationFlow> flow) {
        return this.generateClass(this.prototypeClass(prototype), flow);
    }

    default ClassResult generateSimpleClass(final ClassDescriptor descriptor,
                                            final BiConsumer<ClassDescriptor, ClassGenerationFlow> flow) {
        return this.generateClass(ClassPrototype.simple(descriptor), flow);
    }
}
