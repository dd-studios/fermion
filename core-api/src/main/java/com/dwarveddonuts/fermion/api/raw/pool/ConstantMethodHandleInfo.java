package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantMethodHandleInfo extends CpInfo {
    U1 TAG = CONSTANT_METHOD_HANDLE;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U1 getReferenceKind();
    U2 getReferenceIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantMethodHandleInfo");
        }
    }

    void setReferenceKind(final U1 referenceKind);
    void setReferenceIndex(final U2 referenceIndex);
}
