package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface EmptyTargetTypeAnnotation extends TypeAnnotation {
    @Override
    default Array<U1> getTargetInfo() {
        return Array.of(0);
    }

    @Override
    default void setTargetInfo(final Array<U1> targetInfo) {
        // no-op
    }
}
