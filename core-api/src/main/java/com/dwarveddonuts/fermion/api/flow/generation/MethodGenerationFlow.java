package com.dwarveddonuts.fermion.api.flow.generation;

import com.dwarveddonuts.fermion.api.description.InterfaceDescriptor;
import com.dwarveddonuts.fermion.api.flow.bytecode.BytecodeFlow;
import com.dwarveddonuts.fermion.api.flow.code.CodeFlow;

import java.util.function.Consumer;

public interface MethodGenerationFlow {
    void annotate(final InterfaceDescriptor annotation, final Consumer<AnnotationGenerationFlow.Usage> flow);
    void annotateParameter(final int index, final InterfaceDescriptor annotation,
                           final Consumer<AnnotationGenerationFlow.Usage> flow);

    void code(final Consumer<CodeFlow> flow);

    void bytecode(final Consumer<BytecodeFlow> flow);

    default void annotate(final InterfaceDescriptor annotation) {
        this.annotate(annotation, flow -> {});
    }

    default void annotateParameter(final int index, final InterfaceDescriptor annotation) {
        this.annotateParameter(index, annotation, flow -> {});
    }
}
