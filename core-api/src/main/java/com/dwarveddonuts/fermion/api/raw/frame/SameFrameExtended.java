package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.vmtypes.U1;

public interface SameFrameExtended extends StackMapFrame {
    @Override
    default U1 getFrameType() {
        return SAME_FRAME_EXTENDED;
    }

    @Override
    default void setFrameType(final U1 frameType) {
        if (!SAME_FRAME_EXTENDED.equals(frameType)) {
            throw new IllegalArgumentException("Invalid frame type value for SameFrameExtended");
        }
    }
}
