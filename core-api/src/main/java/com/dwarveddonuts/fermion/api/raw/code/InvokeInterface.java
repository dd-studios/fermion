package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface InvokeInterface extends CodeInstruction {
    U1 OPCODE = INVOKEINTERFACE;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getIndexTop() {
        return this.getParameters().get(0);
    }

    default U1 getIndexBottom() {
        return this.getParameters().get(1);
    }

    default U1 getCount() {
        return this.getParameters().get(2);
    }

    default U1 getFourthParameter() {
        return U1.of(0);
    }

    default U2 getIndex() {
        return U2.of(this.getIndexTop().intValue() << 8 | this.getIndexBottom().intValue());
    }

    @Override
    default byte getLength() {
        return 4;
    }

    default void setIndexTop(final U1 value) {
        this.getParameters().set(0, value);
    }

    default void setIndexBottom(final U1 value) {
        this.getParameters().set(1, value);
    }

    @SuppressWarnings("SpellCheckingInspection")
    default void setCount(final U1 value) {
        if (value.compareTo(U1.of(0)) == 0) {
            throw new IllegalArgumentException("Third parameter for an 'invokeinterface' instruction cannot be 0");
        }
        this.getParameters().set(2, value);
    }

    @SuppressWarnings("SpellCheckingInspection")
    default void setFourthParameter(final U1 value) {
        if (value.compareTo(U1.of(0)) != 0) {
            throw new IllegalArgumentException("Fourth parameter for an 'invokeinterface' instruction can only be 0");
        }
    }

    default void setIndex(final U2 value) {
        this.setIndexTop(U1.of(value.intValue() >> 8));
        this.setIndexBottom(U1.of(value.byteValue()));
    }
}
