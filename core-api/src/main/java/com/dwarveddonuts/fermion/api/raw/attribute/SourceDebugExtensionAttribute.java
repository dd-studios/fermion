package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface SourceDebugExtensionAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "SourceDebugExtension";

    @Override
    default Array<U1> getInfo() {
        return this.getDebugExtension();
    }

    Array<U1> getDebugExtension();

    @Override
    default void setInfo(final Array<U1> info) {
        this.setDebugExtension(info);
    }

    void setDebugExtension(final Array<U1> debugExtension);
}
