package com.dwarveddonuts.fermion.api.raw.verification;

import com.dwarveddonuts.vmtypes.U1;

public interface UninitializedThisVariableInfo extends VerificationTypeInfo {
    U1 TAG = ITEM_UNINITIALIZED_THIS;

    @Override
    default U1 getTag() {
        return TAG;
    }

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for UninitializedThisVariableInfo");
        }
    }
}
