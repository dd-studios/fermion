package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface LocalVariableTypeTableAttribute extends AttributeInfo {
    interface LocalVariableTypeTableElement {
        U2 getStartPc();
        U2 getLength();
        U2 getNameIndex();
        U2 getSignatureIndex();
        U2 getIndex();

        void setStartPc(final U2 startPc);
        void setLength(final U2 length);
        void setNameIndex(final U2 nameIndex);
        void setSignatureIndex(final U2 signatureIndex);
        void setIndex(final U2 index);
    }

    String ATTRIBUTE_NAME = "LocalVariableTypeTable";

    U2 getLocalVariableTypeTableLength();
    Array<LocalVariableTypeTableElement> getLocalVariableTypeTable();

    void setLocalVariableTypeTableLength(final U2 localVariableTypeTableLength);
    void setLocalVariableTypeTable(final Array<LocalVariableTypeTableElement> localVariableTypeTable);
}
