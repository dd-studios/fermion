package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface IInc extends CodeInstruction {
    U1 OPCODE = IINC;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getIndex() {
        return this.getParameters().get(0);
    }

    default byte getConst() {
        return this.getParameters().get(1).byteValue();
    }

    @Override
    default byte getLength() {
        return 2;
    }

    default void setIndex(final U1 value) {
        this.getParameters().set(0, value);
    }

    default void setConst(final byte value) {
        this.getParameters().set(1, U1.of(value));
    }
}
