package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface RecordComponentInfo extends ClassFileSpecPart {
    U2 getNameIndex();
    U2 getDescriptorIndex();
    U2 getAttributesCount();
    Table<AttributeInfo> getAttributes();

    void setNameIndex(final U2 nameIndex);
    void setDescriptorIndex(final U2 descriptorIndex);
    void setAttributesCount(final U2 attributesCount);
    void setAttributes(final Table<AttributeInfo> attributes);
}
