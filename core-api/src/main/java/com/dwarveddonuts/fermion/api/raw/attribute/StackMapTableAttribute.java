package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.frame.StackMapFrame;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface StackMapTableAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "StackMapTable";

    U2 getNumberOfEntries();
    Table<StackMapFrame> getEntries();

    void setNumberOfEntries(final U2 numberOfEntries);
    void setEntries(final Table<StackMapFrame> entries);
}
