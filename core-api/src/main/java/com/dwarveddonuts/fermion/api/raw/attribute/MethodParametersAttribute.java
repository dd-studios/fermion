package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface MethodParametersAttribute extends AttributeInfo {
    interface ParameterEntry {
        U2 getNameIndex();
        U2 getAccessFlags();

        void setNameIndex(final U2 nameIndex);
        void setAccessFlags(final U2 accessFlags);
    }

    String ATTRIBUTE_NAME = "MethodParameters";

    U1 getParametersCount();
    Array<ParameterEntry> getParameters();

    void setParametersCount(final U1 parametersCount);
    void setParameters(final Array<ParameterEntry> parameters);
}
