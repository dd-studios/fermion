package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U4;

public interface SyntheticAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "Synthetic";
    U4 LENGTH = U4.of(0);

    @Override
    default U4 getAttributeLength() {
        return LENGTH;
    }

    @Override
    default Array<U1> getInfo() {
        return Array.of(0);
    }

    @Override
    default void setAttributeLength(final U4 attributeLength) {
        if (!LENGTH.equals(attributeLength)) {
            throw new IllegalArgumentException("Invalid length for " + ATTRIBUTE_NAME + "Attribute");
        }
    }

    @Override
    default void setInfo(final Array<U1> info) {
        // No-op
    }
}
