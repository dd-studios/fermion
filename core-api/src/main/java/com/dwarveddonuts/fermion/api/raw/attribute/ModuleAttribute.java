package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface ModuleAttribute extends AttributeInfo {
    interface RequiresEntry {
        U2 getRequiresCount();
        U2 getRequiresFlags();
        U2 getRequiresVersionIndex();

        void setRequiresCount(final U2 requiresCount);
        void setRequiresFlags(final U2 requiresFlags);
        void setRequiresVersionIndex(final U2 requiresVersionIndex);
    }

    interface ExportsEntry {
        U2 getExportsIndex();
        U2 getExportsFlags();
        U2 getExportsToCount();
        Array<U2> getExportsToIndex();

        void setExportsIndex(final U2 exportsIndex);
        void setExportsFlags(final U2 exportsFlags);
        void setExportsToCount(final U2 exportsToCount);
        void setExportsToIndex(final Array<U2> exportsToIndex);
    }

    interface OpensEntry {
        U2 getOpensIndex();
        U2 getOpensFlags();
        U2 getOpensToCount();
        Array<U2> getOpensToIndex();

        void setOpensIndex(final U2 opensIndex);
        void setOpensFlags(final U2 opensFlags);
        void setOpensToCount(final U2 opensToCount);
        void setOpensToIndex(final Array<U2> opensToIndex);
    }

    interface ProvidesEntry {
        U2 getProvidesIndex();
        U2 getProvidesWithCount();
        Array<U2> getProvidesWithIndex();

        void setProvidesIndex(final U2 providesIndex);
        void setProvidesWithCount(final U2 providesWithCount);
        void setProvidesWithIndex(final Array<U2> providesWithIndex);
    }

    String ATTRIBUTE_NAME = "Module";

    U2 getModuleNameIndex();
    U2 getModuleFlags();
    U2 getModuleVersionIndex();
    U2 getRequiresCount();
    Array<RequiresEntry> getRequires();
    U2 getExportsCount();
    Table<ExportsEntry> getExports();
    U2 getOpensCount();
    Table<OpensEntry> getOpens();
    U2 getUsesCount();
    Array<U2> getUsesIndex();
    U2 getProvidesCount();
    Table<ProvidesEntry> getProvides();

    void setModuleNameIndex(final U2 moduleNameIndex);
    void setModuleFlags(final U2 moduleFlags);
    void setModuleVersionIndex(final U2 moduleVersionIndex);
    void setRequiresCount(final U2 requiresCount);
    void setRequires(final Array<RequiresEntry> requires);
    void setExportsCount(final U2 exportsCount);
    void setExports(final Table<ExportsEntry> exports);
    void setOpensCount(final U2 opensCount);
    void setOpens(final Table<OpensEntry> opens);
    void setUsesCount(final U2 usesCount);
    void setUsesIndex(final U2 usesIndex);
    void setProvidesCount(final U2 providesCount);
    void setProvides(final Table<ProvidesEntry> provides);
}
