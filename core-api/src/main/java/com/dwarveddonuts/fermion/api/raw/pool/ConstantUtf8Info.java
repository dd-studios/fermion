package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantUtf8Info extends CpInfo {
    U1 TAG = CONSTANT_UTF8;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getLength();
    Array<U1> getBytes();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantUtf8Info");
        }
    }

    void setLength(final U2 length);
    void setBytes(final Array<U1> bytes);
}
