package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface RecordAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "Record";

    U2 getComponentsCount();
    Table<RecordComponentInfo> getComponents();

    void setComponentsCount(final U2 componentsCount);
    void setComponents(final Table<RecordComponentInfo> components);
}
