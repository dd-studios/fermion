package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.annotation.type.TypeAnnotation;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface RuntimeInvisibleTypeAnnotationsAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "RuntimeInvisibleTypeAnnotations";

    U2 getNumAnnotations();
    Table<TypeAnnotation> getAnnotations();

    void setNumAnnotations(final U2 numAnnotations);
    void setAnnotations(final Table<TypeAnnotation> annotations);
}
