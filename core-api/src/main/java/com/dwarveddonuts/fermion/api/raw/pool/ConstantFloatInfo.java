package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U4;

public interface ConstantFloatInfo extends CpInfo {
    U1 TAG = CONSTANT_FLOAT;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U4 getBytes();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantFloatInfo");
        }
    }

    void setBytes(final U4 bytes);
}

