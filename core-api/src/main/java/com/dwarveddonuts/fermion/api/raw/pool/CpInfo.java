package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface CpInfo extends ClassFileSpecPart {
    U1 CONSTANT_CLASS = U1.of(7);
    U1 CONSTANT_FIELD_REF = U1.of(9);
    U1 CONSTANT_METHOD_REF = U1.of(10);
    U1 CONSTANT_INTERFACE_METHOD_REF = U1.of(11);
    U1 CONSTANT_STRING = U1.of(8);
    U1 CONSTANT_INTEGER = U1.of(3);
    U1 CONSTANT_FLOAT = U1.of(4);
    U1 CONSTANT_LONG = U1.of(5);
    U1 CONSTANT_DOUBLE = U1.of(6);
    U1 CONSTANT_NAME_AND_TYPE = U1.of(12);
    U1 CONSTANT_UTF8 = U1.of(1);
    U1 CONSTANT_METHOD_HANDLE = U1.of(15);
    U1 CONSTANT_METHOD_TYPE = U1.of(16);
    U1 CONSTANT_DYNAMIC = U1.of(17);
    U1 CONSTANT_INVOKE_DYNAMIC = U1.of(18);
    U1 CONSTANT_MODULE = U1.of(19);
    U1 CONSTANT_PACKAGE = U1.of(20);

    U1 getTag();
    Array<U1> getInfo();

    void setTag(final U1 tag);
}
