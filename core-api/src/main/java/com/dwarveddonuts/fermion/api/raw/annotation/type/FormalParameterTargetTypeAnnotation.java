package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U1;

public interface FormalParameterTargetTypeAnnotation extends TypeAnnotation {
    U1 getFormalParameterIndex();

    void setFormalParameterIndex(final U1 formalParameterIndex);
}
