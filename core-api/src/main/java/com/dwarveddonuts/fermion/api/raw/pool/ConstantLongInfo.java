package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U4;

public interface ConstantLongInfo extends CpInfo {
    U1 TAG = CONSTANT_LONG;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U4 getHighBytes();
    U4 getLowBytes();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantLongInfo");
        }
    }

    void setHighBytes(final U4 highBytes);
    void setLowBytes(final U4 lowBytes);
}
