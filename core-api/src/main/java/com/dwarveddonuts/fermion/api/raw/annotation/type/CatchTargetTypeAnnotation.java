package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U2;

public interface CatchTargetTypeAnnotation extends TypeAnnotation {
    U2 getExceptionTableIndex();

    void setExceptionTableIndex(final U2 exceptionTableIndex);
}
