package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface LookupSwitch extends CodeInstruction {
    interface MatchOffsetPair {
        U1 getMatchFirst();
        U1 getMatchSecond();
        U1 getMatchThird();
        U1 getMatchFourth();
        U1 getOffsetFirst();
        U1 getOffsetSecond();
        U1 getOffsetThird();
        U1 getOffsetFourth();

        default int getMatch() {
            final int first = this.getMatchFirst().byteValue();
            final int second = this.getMatchSecond().intValue();
            final int third = this.getMatchThird().intValue();
            final int fourth = this.getMatchFourth().intValue();
            return (first << 24) | (second << 16) | (third << 8) | fourth;
        }

        default int getOffset() {
            final int first = this.getOffsetFirst().byteValue();
            final int second = this.getOffsetSecond().intValue();
            final int third = this.getOffsetThird().intValue();
            final int fourth = this.getOffsetFourth().intValue();
            return (first << 24) | (second << 16) | (third << 8) | fourth;
        }
    }

    U1 OPCODE = LOOKUPSWITCH;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    byte getPaddingSize();

    default U1 getDefaultFirst() {
        return this.getParameters().get(this.getPaddingSize());
    }

    default U1 getDefaultSecond() {
        return this.getParameters().get(this.getPaddingSize() + 1);
    }

    default U1 getDefaultThird() {
        return this.getParameters().get(this.getPaddingSize() + 2);
    }

    default U1 getDefaultFourth() {
        return this.getParameters().get(this.getPaddingSize() + 3);
    }

    default U1 getNPairsFirst() {
        return this.getParameters().get(this.getPaddingSize() + 4);
    }

    default U1 getNPairsSecond() {
        return this.getParameters().get(this.getPaddingSize() + 5);
    }

    default U1 getNPairsThird() {
        return this.getParameters().get(this.getPaddingSize() + 6);
    }

    default U1 getNPairsFourth() {
        return this.getParameters().get(this.getPaddingSize() + 7);
    }

    default int getDefault() {
        final int first = this.getDefaultFirst().byteValue();
        final int second = this.getDefaultSecond().intValue();
        final int third = this.getDefaultThird().intValue();
        final int fourth = this.getDefaultFourth().intValue();
        return (first << 24) | (second << 16) | (third << 8) | fourth;
    }

    default int getNPairs() {
        final int first = this.getNPairsFirst().byteValue();
        final int second = this.getNPairsSecond().intValue();
        final int third = this.getNPairsThird().intValue();
        final int fourth = this.getNPairsFourth().intValue();
        return (first << 24) | (second << 16) | (third << 8) | fourth;
    }

    MatchOffsetPair getMatchOffsetPairAt(final int position);

    @Override
    default byte getLength() {
        return (byte) (8 + this.getPaddingSize() + 8 * this.getNPairs());
    }

    void setPaddingSize(final byte value);

    default void setDefaultFirst(final U1 value) {
        this.getParameters().set(this.getPaddingSize(), value);
    }

    default void setDefaultSecond(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 1, value);
    }

    default void setDefaultThird(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 2, value);
    }

    default void setDefaultFourth(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 3, value);
    }

    default void setNPairsFirst(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 4, value);
    }

    default void setNPairsSecond(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 5, value);
    }

    default void setNPairsThird(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 6, value);
    }

    default void setNPairsFourth(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 7, value);
    }

    default void setDefault(final int value) {
        this.setDefaultFirst(U1.of((byte) (value >> 24)));
        this.setDefaultSecond(U1.of((byte) ((value >> 16) & 0xFF)));
        this.setDefaultThird(U1.of((byte) ((value >> 8) & 0xFF)));
        this.setDefaultFourth(U1.of((byte) (value & 0xFF)));
    }

    default void setNPairs(final int value) {
        this.setNPairsFirst(U1.of((byte) (value >> 24)));
        this.setNPairsSecond(U1.of((byte) ((value >> 16) & 0xFF)));
        this.setNPairsThird(U1.of((byte) ((value >> 8) & 0xFF)));
        this.setNPairsFourth(U1.of((byte) (value & 0xFF)));
    }

    void setMatchOffsetPairAt(final int position, final MatchOffsetPair pair);
}
