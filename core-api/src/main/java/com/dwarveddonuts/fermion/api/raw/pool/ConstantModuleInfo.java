package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantModuleInfo extends CpInfo {
    U1 TAG = CONSTANT_MODULE;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getNameIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantModuleInfo");
        }
    }

    void setNameIndex(final U2 nameIndex);
}
