package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantStringInfo extends CpInfo {
    U1 TAG = CONSTANT_STRING;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getStringIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantStringInfo");
        }
    }

    void setStringIndex(final U2 stringIndex);
}
