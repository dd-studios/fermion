package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantInvokeDynamicInfo extends CpInfo {
    U1 TAG = CONSTANT_INVOKE_DYNAMIC;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getBootstrapMethodAttrIndex();
    U2 getNameAndTypeIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantInvokeDynamicInfo");
        }
    }

    void setBootstrapMethodAttrIndex(final U2 bootstrapMethodAttrIndex);
    void setNameAndTypeIndex(final U2 nameAndTypeIndex);
}
