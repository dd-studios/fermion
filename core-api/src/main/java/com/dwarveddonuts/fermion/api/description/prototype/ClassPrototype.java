package com.dwarveddonuts.fermion.api.description.prototype;

import com.dwarveddonuts.fermion.api.description.AccessModifier;
import com.dwarveddonuts.fermion.api.description.ClassDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public final class ClassPrototype {
    public enum Modifier {
        ABSTRACT,
        STRICT_FP,
        FINAL,
        SYNTHETIC
    }

    private final AccessModifier modifier;
    private final ClassDescriptor name;
    private final ClassDescriptor superClass;
    private final Collection<ClassDescriptor> interfaces;
    private final Set<Modifier> modifiers;

    private ClassPrototype(final AccessModifier modifier, final ClassDescriptor name, final ClassDescriptor superClass,
                           final Collection<ClassDescriptor> interfaces, final Set<Modifier> modifiers) {
        this.modifier = modifier;
        this.name = name;
        this.superClass = superClass;
        this.interfaces = interfaces;
        this.modifiers = modifiers;
    }

    public static ClassPrototype simple(final ClassDescriptor name) {
        return simple(AccessModifier.PUBLIC, name);
    }

    public static ClassPrototype simple(final AccessModifier modifier, final ClassDescriptor name) {
        return of(modifier, name, ClassDescriptor.OBJECT, Collections.emptyList(), Modifier.FINAL);
    }

    public static ClassPrototype of(final AccessModifier modifier, final ClassDescriptor name,
                                    final ClassDescriptor superClass, final ClassDescriptor... interfaces) {
        return of(modifier, name, superClass, Arrays.asList(interfaces));
    }

    public static ClassPrototype of(final AccessModifier modifier, final ClassDescriptor name,
                                    final ClassDescriptor superClass, final Collection<ClassDescriptor> interfaces,
                                    final Modifier... additionalModifiers) {
        Objects.requireNonNull(modifier, "Modifier cannot be null: use 'DEFAULT' instead");
        Objects.requireNonNull(name, "Unable to create a class with a null name");
        Objects.requireNonNull(interfaces, "Unable to create a class with a null super-interface list");
        Objects.requireNonNull(additionalModifiers, "Error in vararg: must not be a null array");

        if (ClassDescriptor.OBJECT.equals(name)) {
            validateObjectConstraints(superClass, interfaces, modifier, additionalModifiers);
        } else {
            validateCommonConstraints(name, superClass, interfaces, modifier, additionalModifiers);
        }

        final var modifierSet = EnumSet.noneOf(Modifier.class);
        modifierSet.addAll(Arrays.asList(additionalModifiers));
        return new ClassPrototype(modifier, name, superClass, new ArrayList<>(interfaces), modifierSet);
    }

    private static void validateObjectConstraints(final ClassDescriptor superClass,
                                                  final Collection<ClassDescriptor> interfaces,
                                                  final AccessModifier mod, final Modifier... additional) {
        if (superClass != null) {
            throw new IllegalArgumentException("Object class must have no superclass");
        }
        if (interfaces.size() != 0) {
            throw new IllegalArgumentException("Object class must not inherit from any interface");
        }
        if (mod != AccessModifier.PUBLIC) {
            throw new IllegalArgumentException("Object class must be public");
        }
        if (additional.length != 0) {
            throw new IllegalArgumentException("Unable to set additional modifiers to Object class");
        }
    }

    private static void validateCommonConstraints(final ClassDescriptor name, final ClassDescriptor superClass,
                                                  final Collection<ClassDescriptor> interfaces,
                                                  final AccessModifier access, final Modifier... mods) {
        if (access != AccessModifier.PUBLIC && access != AccessModifier.DEFAULT) {
            throw new IllegalArgumentException("Modifier '" + access + "' cannot be used in a class prototype");
        }

        validateDesc(name, "create a class for");
        validateDesc(Objects.requireNonNull(superClass, "Class must have a superclass"), "extend");
        interfaces.forEach(it -> validateDesc(it, "implement"));

        if (interfaces.stream().distinct().count() != interfaces.size()) {
            throw new IllegalArgumentException("Illegal duplicates in " + interfaces);
        }

        validateModifiers(mods);
    }

    private static void validateDesc(final ClassDescriptor descriptor, final String error) {
        if (descriptor.isArray()) {
            throw new IllegalArgumentException("Unable to " + error + " array type " + descriptor);
        }
        if (descriptor.isPrimitive()) {
            throw new IllegalArgumentException("Unable to " + error + " primitive type " + descriptor);
        }
    }

    private static void validateModifiers(final Modifier... modifiers) {
        if (Arrays.stream(modifiers).distinct().count() != modifiers.length) {
            throw new IllegalArgumentException("Illegal duplicates in " + Arrays.toString(modifiers));
        }

        /*mutable*/ var hasAbstract = false;
        /*mutable*/ var hasFinal = false;
        for (final var modifier : modifiers) {
            if (modifier == Modifier.ABSTRACT) hasAbstract = true;
            else if (modifier == Modifier.FINAL) hasFinal = true;
        }

        if (hasAbstract && hasFinal) {
            throw new IllegalArgumentException("Unable to set both final and abstract as class prototype modifiers");
        }
    }

    public AccessModifier getAccessModifier() {
        return this.modifier;
    }

    public ClassDescriptor getName() {
        return this.name;
    }

    public ClassDescriptor getSuperClass() {
        return this.superClass;
    }

    public Collection<ClassDescriptor> getSuperInterfaces() {
        return Collections.unmodifiableCollection(this.interfaces);
    }

    public Set<Modifier> getModifiers() {
        return Collections.unmodifiableSet(this.modifiers);
    }

    @Override
    public String toString() {
        return "ClassPrototype{" +
                "modifier=" + this.modifier +
                ", name=" + this.name +
                ", superClass=" + this.superClass +
                ", interfaces=" + this.interfaces +
                ", modifiers=" + this.modifiers +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (ClassPrototype) o;
        return this.modifier == that.modifier &&
                Objects.equals(this.name, that.name) &&
                Objects.equals(this.superClass, that.superClass) &&
                this.interfaces.equals(that.interfaces) &&
                this.modifiers.equals(that.modifiers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.modifier, this.name, this.superClass, this.interfaces, this.modifiers);
    }
}
