package com.dwarveddonuts.fermion.api.description;

import java.util.Objects;

public final class ComponentDescriptor {
    private final ClassDescriptor descriptor;
    private final GenericDescriptor pureGeneric;

    private ComponentDescriptor(final ClassDescriptor descriptor, final GenericDescriptor pureGeneric) {
        this.descriptor = descriptor;
        this.pureGeneric = pureGeneric;
    }

    // TODO("Public?")
    static ComponentDescriptor ofClass(final ClassDescriptor descriptor) {
        Objects.requireNonNull(descriptor, "Unable to create a component descriptor for a null class");
        return new ComponentDescriptor(descriptor, null);
    }

    // TODO("Public?")
    static ComponentDescriptor ofGeneric(final GenericDescriptor descriptor) {
        Objects.requireNonNull(descriptor, "Unable to create a component descriptor for a null generic");
        return new ComponentDescriptor(null, descriptor);
    }

    public boolean isPureGeneric() {
        return this.pureGeneric != null;
    }

    public boolean isClassBased() {
        return this.descriptor != null;
    }

    public ClassDescriptor getDescriptor() {
        if (!this.isClassBased()) {
            throw new IllegalStateException("The current descriptor does not describe a class component");
        }
        return this.descriptor;
    }

    public GenericDescriptor getPureGeneric() {
        if (!this.isPureGeneric()) {
            throw new IllegalStateException("The current descriptor does not describe a pure generic component");
        }
        return this.pureGeneric;
    }

    public String getErasedBytecodeName() {
        // TODO("Don't erase to object, but defer to pureGeneric")
        return (this.isPureGeneric()? ClassDescriptor.OBJECT : this.descriptor).getErasedBytecodeName();
    }

    public String getBytecodeName() {
        // TODO("")
        return null;
    }

    @Override
    public String toString() {
        return this.getBytecodeName();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (ComponentDescriptor) o;
        return Objects.equals(this.descriptor, that.descriptor)
                && Objects.equals(this.pureGeneric, that.pureGeneric);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.descriptor, this.pureGeneric);
    }
}
