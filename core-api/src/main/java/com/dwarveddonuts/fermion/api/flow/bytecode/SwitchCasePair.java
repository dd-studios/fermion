package com.dwarveddonuts.fermion.api.flow.bytecode;

import java.util.Objects;

public final class SwitchCasePair {
    private final int match;
    private final Label jumpTarget;

    private SwitchCasePair(final int match, final Label jumpTarget) {
        this.match = match;
        this.jumpTarget = jumpTarget;
    }

    public static SwitchCasePair of(final int match, final Label jumpTarget) {
        return new SwitchCasePair(match, jumpTarget);
    }

    public int getMatch() {
        return this.match;
    }

    public Label getJumpTarget() {
        return this.jumpTarget;
    }

    @Override
    public String toString() {
        return this.match + " --> " + this.jumpTarget;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final var that = (SwitchCasePair) o;
        return this.match == that.match && Objects.equals(this.jumpTarget, that.jumpTarget);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.match, this.jumpTarget);
    }
}
