package com.dwarveddonuts.fermion.api.flow.bytecode;

public enum ArrayType {
    BOOLEAN,
    CHAR,
    FLOAT,
    DOUBLE,
    BYTE,
    SHORT,
    INT,
    LONG
}
