package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantInterfaceMethodRefInfo extends CpInfo {
    U1 TAG = CONSTANT_INTERFACE_METHOD_REF;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getClassIndex();
    U2 getNameAndTypeIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantInterfaceMethodRefInfo");
        }
    }

    void setClassIndex(final U2 classIndex);
    void setNameAndTypeIndex(final U2 nameAndTypeIndex);
}
