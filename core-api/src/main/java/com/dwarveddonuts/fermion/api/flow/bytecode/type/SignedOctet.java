package com.dwarveddonuts.fermion.api.flow.bytecode.type;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class SignedOctet extends Number implements Comparable<SignedOctet> {
    private static final class Cache {
        private static final SignedOctet[] CACHE = new SignedOctet[MAX_SIGNED_OCTET + 1];

        static {
            IntStream.rangeClosed(MIN_SIGNED_OCTET, MAX_SIGNED_OCTET)
                    .forEach(it -> CACHE[it - MIN_SIGNED_OCTET] = new SignedOctet((byte) it));
        }
    }

    @Native public static final byte MAX_SIGNED_OCTET = Byte.MAX_VALUE;
    @Native public static final byte MIN_SIGNED_OCTET = Byte.MIN_VALUE;

    public static final int SIZE = Byte.SIZE;
    public static final int BYTES = Byte.BYTES;

    private final byte value;

    private SignedOctet(final byte value) {
        this.value = value;
    }

    public static SignedOctet of(final byte value) {
        return Cache.CACHE[value - MIN_SIGNED_OCTET];
    }

    public static SignedOctet of(final int value) {
        if (value < MIN_SIGNED_OCTET || value > MAX_SIGNED_OCTET) {
            throw new IllegalArgumentException(
                    value + "is outside bounds for signed octet: [" + MIN_SIGNED_OCTET + ", " + MAX_SIGNED_OCTET + "]"
            );
        }
        return of((byte) value);
    }

    public SignedOctet add(final SignedOctet other) {
        return of((byte) (this.value + other.value));
    }

    public SignedOctet subtract(final SignedOctet other) {
        return of((byte) (this.value - other.value));
    }

    public SignedOctet multiply(final SignedOctet other) {
        return of((byte) (this.value * other.value));
    }

    public SignedOctet divide(final SignedOctet other) {
        return of((byte) (this.longValue() / other.longValue()));
    }

    @Override
    public int compareTo(@NotNull final SignedOctet that) {
        return Byte.compare(this.value, that.value);
    }

    @Override
    public byte byteValue() {
        return this.value;
    }

    @Override
    public short shortValue() {
        return this.value;
    }

    @Override
    public int intValue() {
        return this.value;
    }

    @Override
    public long longValue() {
        return this.value;
    }

    @Override
    public float floatValue() {
        return this.value;
    }

    @Override
    public double doubleValue() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return Byte.hashCode(this.value);
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof SignedOctet && this.value == ((SignedOctet) obj).value);
    }

    @Override
    public String toString() {
        return Byte.toString(this.value);
    }
}
