package com.dwarveddonuts.fermion.api.description;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class MethodDescriptor {
    public static final class Builder {
        private final String methodName;
        private final List<GenericDescriptor> genericParameters;
        private final List<ComponentDescriptor> methodParameters;
        private ComponentDescriptor returnType;

        private Builder(final String methodName) {
            this.methodName = methodName;
            this.genericParameters = new ArrayList<>();
            this.methodParameters = new ArrayList<>();
        }

        public Builder returnType(final ClassDescriptor returnType) {
            this.returnType = ComponentDescriptor.ofClass(returnType);
            return this;
        }

        public Builder returnType(final GenericDescriptor returnType) {
            this.returnType = ComponentDescriptor.ofGeneric(returnType);
            return this;
        }

        public Builder parameter(final ClassDescriptor parameter) {
            this.methodParameters.add(ComponentDescriptor.ofClass(parameter));
            return this;
        }

        public Builder parameter(final GenericDescriptor parameter) {
            this.methodParameters.add(ComponentDescriptor.ofGeneric(parameter));
            return this;
        }

        public Builder typeParameter(final GenericDescriptor parameter) {
            this.genericParameters.add(parameter);
            return this;
        }

        public MethodDescriptor make() {
            // TODO("Checks: on name, on generic types, on return type, on parameters...")
            return new MethodDescriptor(this.methodName, new ArrayList<>(this.methodParameters),
                    this.returnType, new ArrayList<>(this.genericParameters));
        }

        private Builder parameters(final List<ClassDescriptor> parameters) {
            parameters.forEach(this::parameter);
            return this;
        }
    }

    private final String methodName;
    private final List<ComponentDescriptor> methodParameters;
    private final ComponentDescriptor returnType;
    private final List<GenericDescriptor> genericParameters;

    private MethodDescriptor(final String methodName, final List<ComponentDescriptor> methodParameters,
                             final ComponentDescriptor returnType,
                             final List<GenericDescriptor> genericParameters) {
        this.methodName = methodName;
        this.methodParameters = methodParameters;
        this.returnType = returnType;
        this.genericParameters = genericParameters;
    }

    public static MethodDescriptor of(final String methodName, final ClassDescriptor returnType,
                                      final ClassDescriptor... parameters) {
        Objects.requireNonNull(methodName, "Unable to create method descriptor with a null name");
        Objects.requireNonNull(returnType, "Unable to create method descriptor with a null return type");
        Objects.requireNonNull(parameters, "Unable to create method descriptor with null parameters");
        return of(methodName, returnType, Arrays.asList(parameters));
    }

    public static MethodDescriptor of(final String methodName, final ClassDescriptor returnType,
                                      final List<ClassDescriptor> parameters) {
        Objects.requireNonNull(methodName, "Unable to create method descriptor with a null name");
        Objects.requireNonNull(returnType, "Unable to create method descriptor with a null return type");
        Objects.requireNonNull(parameters, "Unable to create method descriptor with null parameters");
        return builder(methodName).returnType(returnType).parameters(parameters).make();
    }

    public static MethodDescriptor.Builder builder(final String methodName) {
        return new Builder(methodName);
    }

    public String getMethodName() {
        return this.methodName;
    }

    public String getErasedBytecodeDescriptor() {
        // (Ljava/lang/Object;Ljava/util/List;)I
        return String.format(
                "(%s)%s",
                this.methodParameters.stream()
                        .map(ComponentDescriptor::getErasedBytecodeName)
                        .collect(Collectors.joining(", ")),
                this.returnType.getErasedBytecodeName()
        );
    }

    public String getBytecodeDescriptor() {
        // TODO("(TE;Ljava/util/List<Ljava/lang/Integer;>;)I")
        return null;
    }

    public String getBytecodeSignature() {
        // TODO("<E:Ljava/lang/Object;>")
        return null;
    }

    public String getFullBytecodeSignature() {
        // TODO("<E:Ljava/lang/Object;>(TE;Ljava/util/List<Ljava/lang/Integer;>;)I")
        return null;
    }

    public int getParameterSize() {
        return this.methodParameters.size();
    }

    public List<ComponentDescriptor> getParameters() {
        return Collections.unmodifiableList(this.methodParameters);
    }

    public ComponentDescriptor getReturnType() {
        return this.returnType;
    }

    public boolean isGenericMethod() {
        return !this.genericParameters.isEmpty();
    }

    public List<GenericDescriptor> getTypeParameters() {
        if (!this.isGenericMethod()) {
            throw new IllegalStateException("The current descriptor does not describe a generic method");
        }
        return Collections.unmodifiableList(this.genericParameters);
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.methodName, this.getFullBytecodeSignature());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final MethodDescriptor that = (MethodDescriptor) o;
        return this.methodName.equals(that.methodName) && this.methodParameters.equals(that.methodParameters)
                && this.returnType.equals(that.returnType) && this.genericParameters.equals(that.genericParameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.methodName, this.methodParameters, this.returnType, this.genericParameters);
    }
}
