package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface AStore2 extends CodeInstruction {
    U1 OPCODE = ASTORE_2;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    @Override
    default Array<U1> getParameters() {
        return Array.of(0);
    }

    @Override
    default byte getLength() {
        return 0;
    }
}
