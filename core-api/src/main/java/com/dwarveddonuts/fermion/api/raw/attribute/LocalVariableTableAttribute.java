package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface LocalVariableTableAttribute extends AttributeInfo {
    interface LocalVariableTableElement {
        U2 getStartPc();
        U2 getLength();
        U2 getNameIndex();
        U2 getDescriptorIndex();
        U2 getIndex();

        void setStartPc(final U2 startPc);
        void setLength(final U2 length);
        void setNameIndex(final U2 nameIndex);
        void setDescriptorIndex(final U2 descriptorIndex);
        void setIndex(final U2 index);
    }

    String ATTRIBUTE_NAME = "LocalVariableTable";

    U2 getLocalVariableTableLength();
    Array<LocalVariableTableElement> getLocalVariableTable();

    void setLocalVariableTableLength(final U2 localVariableTableLength);
    void setLocalVariableTable(final Array<LocalVariableTableElement> localVariableTable);
}
