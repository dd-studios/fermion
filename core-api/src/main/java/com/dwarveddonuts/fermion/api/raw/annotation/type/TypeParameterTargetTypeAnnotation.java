package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U1;

public interface TypeParameterTargetTypeAnnotation extends TypeAnnotation {
    U1 getTypeParameterIndex();

    void setTypeParameterIndex(final U1 typeParameterIndex);
}
