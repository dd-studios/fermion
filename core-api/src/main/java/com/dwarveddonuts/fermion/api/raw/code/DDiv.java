package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface DDiv extends CodeInstruction {
    U1 OPCODE = DDIV;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    @Override
    default Array<U1> getParameters() {
        return Array.of(0);
    }

    @Override
    default byte getLength() {
        return 0;
    }
}
