package com.dwarveddonuts.fermion.api.raw;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface FieldInfo extends ClassFileSpecPart {
    U2 getAccessFlags();
    U2 getNameIndex();
    U2 getDescriptorIndex();
    U2 getAttributesCount();
    Table<AttributeInfo> getAttributes();

    void setAccessFlags(final U2 accessFlags);
    void setNameIndex(final U2 nameIndex);
    void setDescriptorIndex(final U2 descriptorIndex);
    void setAttributesCount(final U2 attributesCount);
    void setAttributes(final Table<AttributeInfo> attributes);
}
