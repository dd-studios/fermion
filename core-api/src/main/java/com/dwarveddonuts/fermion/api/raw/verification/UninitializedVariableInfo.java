package com.dwarveddonuts.fermion.api.raw.verification;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface UninitializedVariableInfo extends VerificationTypeInfo {
    U1 TAG = ITEM_UNINITIALIZED;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getOffset();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ObjectVariableInfo");
        }
    }

    void setOffset(final U2 offset);
}
