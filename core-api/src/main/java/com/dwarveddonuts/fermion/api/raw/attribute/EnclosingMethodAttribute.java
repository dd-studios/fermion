package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.U2;
import com.dwarveddonuts.vmtypes.U4;

public interface EnclosingMethodAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "EnclosingMethod";
    U4 LENGTH = U4.of(4);

    @Override
    default U4 getAttributeLength() {
        return LENGTH;
    }

    U2 getClassIndex();
    U2 getMethodIndex();

    @Override
    default void setAttributeLength(final U4 attributeLength) {
        if (!LENGTH.equals(attributeLength)) {
            throw new IllegalArgumentException("Invalid length for " + ATTRIBUTE_NAME + "Attribute");
        }
    }

    void setClassIndex(final U2 classIndex);
    void setMethodIndex(final U2 methodIndex);
}
