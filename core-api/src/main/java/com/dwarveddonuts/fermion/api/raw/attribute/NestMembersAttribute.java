package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface NestMembersAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "NestMembers";

    U2 getNumberOfClasses();
    Array<U2> getClasses();

    void setNumberOfClasses(final U2 numberOfClasses);
    void setClasses(final Array<U2> classes);
}
