package com.dwarveddonuts.fermion.api.flow.generation;

import com.dwarveddonuts.fermion.api.description.InterfaceDescriptor;

import java.util.function.Consumer;

public interface FieldGenerationFlow {
    void annotate(final InterfaceDescriptor annotation, final Consumer<AnnotationGenerationFlow.Usage> flow);

    default void annotate(final InterfaceDescriptor annotation) {
        this.annotate(annotation, flow -> {});
    }
}
