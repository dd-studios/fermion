package com.dwarveddonuts.fermion.api.flow.bytecode;

import com.dwarveddonuts.fermion.api.flow.bytecode.type.SignedDoubleOctet;
import com.dwarveddonuts.fermion.api.flow.bytecode.type.UnsignedDoubleOctet;

@SuppressWarnings("SpellCheckingInspection")
public interface WideBytecodeFlow {
    void iload(final UnsignedDoubleOctet localIndex);
    void lload(final UnsignedDoubleOctet localIndex);
    void fload(final UnsignedDoubleOctet localIndex);
    void dload(final UnsignedDoubleOctet localIndex);
    void aload(final UnsignedDoubleOctet localIndex);
    void istore(final UnsignedDoubleOctet localIndex);
    void lstore(final UnsignedDoubleOctet localIndex);
    void fstore(final UnsignedDoubleOctet localIndex);
    void dstore(final UnsignedDoubleOctet localIndex);
    void astore(final UnsignedDoubleOctet localIndex);
    void iinc(final UnsignedDoubleOctet localIndex, final SignedDoubleOctet constant);
    void ret(final Label returnTarget);
}
