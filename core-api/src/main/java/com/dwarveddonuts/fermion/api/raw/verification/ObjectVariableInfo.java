package com.dwarveddonuts.fermion.api.raw.verification;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ObjectVariableInfo extends VerificationTypeInfo {
    U1 TAG = ITEM_OBJECT;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getCPoolIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ObjectVariableInfo");
        }
    }

    void setCPoolIndex(final U2 cPoolIndex);
}
