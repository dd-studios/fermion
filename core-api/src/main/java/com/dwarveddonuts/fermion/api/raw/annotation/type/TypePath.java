package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface TypePath extends ClassFileSpecPart {
    interface TypePathEntry {
        U1 getTypePathKind();
        U1 getTypeArgumentIndex();

        void setTypePathKind(final U1 typePathKind);
        void setTypeArgumentIndex(final U1 typeArgumentIndex);
    }

    U1 getPathLength();
    Array<TypePathEntry> getPath();

    void setPathLength(final U1 pathLength);
    void setPath(final Array<TypePathEntry> path);
}
