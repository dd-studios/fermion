package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ArrayValue extends ElementValue {
    @Override
    default U1 getTag() {
        return ARRAY;
    }

    U2 getNumValues();
    Table<ElementValue> getValues();

    @Override
    default void setTag(final U1 tag) {
        if (!ARRAY.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ArrayValue");
        }
    }

    void setNumValues(final U2 numValues);
    void setValues(final Table<ElementValue> values);
}
