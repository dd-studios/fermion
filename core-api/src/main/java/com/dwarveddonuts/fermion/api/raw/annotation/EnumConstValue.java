package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface EnumConstValue extends ElementValue {
    @Override
    default U1 getTag() {
        return ENUM;
    }

    U2 getTypeNameIndex();
    U2 getConstNameIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!ENUM.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for EnumConstValue");
        }
    }

    void setTypeNameIndex(final U2 typeNameIndex);
    void setConstNameIndex(final U2 constNameIndex);
}
