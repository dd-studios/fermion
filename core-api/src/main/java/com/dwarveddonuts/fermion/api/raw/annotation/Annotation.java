package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface Annotation extends ClassFileSpecPart {
    interface ElementValuePair {
        U2 getElementNameIndex();
        ElementValue getValue();

        void setElementNameIndex(final U2 elementNameIndex);
        void setValue(final ElementValue value);
    }

    U2 getTypeIndex();
    U2 getNumElementValuePairs();
    Table<ElementValuePair> getElementValuePairs();

    void setTypeIndex(final U2 typeIndex);
    void setNumElementValuePairs(final U2 numElementValuePairs);
    void setElementValuePairs(final Table<ElementValuePair> elementValuePairs);
}
