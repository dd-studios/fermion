package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface InnerClassesAttribute extends AttributeInfo {
    interface InnerClassElement {
        U2 getInnerClassInfoIndex();
        U2 getOuterClassInfoIndex();
        U2 getInnerNameIndex();
        U2 getInnerClassAccessFlags();

        void setInnerClassInfoIndex(final U2 innerClassInfoIndex);
        void setOuterClassInfoIndex(final U2 outerClassInfoIndex);
        void setInnerNameIndex(final U2 innerNameIndex);
        void setInnerClassAccessFlags(final U2 innerClassAccessFlags);
    }

    String ATTRIBUTE_NAME = "InnerClasses";

    U2 getNumberOfClasses();
    Array<InnerClassElement> getClasses();

    void setNumberOfClasses(final U2 numberOfClasses);
    void setClasses(final Array<InnerClassElement> elements);
}
