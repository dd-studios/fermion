package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.annotation.Annotation;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface RuntimeVisibleAnnotationsAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "RuntimeVisibleAnnotations";

    U2 getNumAnnotations();
    Table<Annotation> getAnnotations();

    void setNumAnnotations(final U2 numAnnotations);
    void setAnnotations(final Table<Annotation> annotations);
}
