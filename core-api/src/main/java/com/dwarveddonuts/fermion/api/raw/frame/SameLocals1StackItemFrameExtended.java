package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.fermion.api.raw.verification.VerificationTypeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface SameLocals1StackItemFrameExtended extends StackMapFrame {
    @Override
    default U1 getFrameType() {
        return SAME_LOCALS_1_STACK_ITEM_EXTENDED;
    }

    Array<VerificationTypeInfo> getStack();

    @Override
    default void setFrameType(final U1 frameType) {
        if (!SAME_LOCALS_1_STACK_ITEM_EXTENDED.equals(frameType)) {
            throw new IllegalArgumentException("Invalid frame type value for SameLocals1StackItemFrameExtended");
        }
    }

    void setStack(final Array<VerificationTypeInfo> stack);
}
