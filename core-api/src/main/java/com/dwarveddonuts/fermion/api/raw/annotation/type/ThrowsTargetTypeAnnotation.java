package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U2;

public interface ThrowsTargetTypeAnnotation extends TypeAnnotation {
    U2 getThrowsTypeIndex();

    void setThrowsTypeIndex(final U2 throwsTypeIndex);
}
