package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.fermion.api.raw.verification.VerificationTypeInfo;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface FullFrame extends StackMapFrame {
    @Override
    default U1 getFrameType() {
        return FULL_FRAME;
    }

    U2 getNumberOfLocals();
    Table<VerificationTypeInfo> getLocals();
    U2 getNumberOfStackItems();
    Table<VerificationTypeInfo> getStack();

    @Override
    default void setFrameType(final U1 frameType) {
        if (!FULL_FRAME.equals(frameType)) {
            throw new IllegalArgumentException("Invalid frame type value for FullFrame");
        }
    }

    void setNumberOfLocals(final U2 numberOfLocals);
    void setLocals(final Table<VerificationTypeInfo> locals);
    void setNumberOfStackItems(final U2 numberOfStackItems);
    void setStack(final Table<VerificationTypeInfo> stack);
}
