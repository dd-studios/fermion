package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface IStore extends CodeInstruction {
    U1 OPCODE = ISTORE;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getIndex() {
        return this.getParameters().get(0);
    }

    @Override
    default byte getLength() {
        return 1;
    }

    default void setIndex(final U1 value) {
        this.getParameters().set(0, value);
    }
}
