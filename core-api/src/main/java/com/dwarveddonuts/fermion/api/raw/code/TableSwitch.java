package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface TableSwitch extends CodeInstruction {
    U1 OPCODE = TABLESWITCH;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    byte getPaddingSize();

    default U1 getDefaultFirst() {
        return this.getParameters().get(this.getPaddingSize());
    }

    default U1 getDefaultSecond() {
        return this.getParameters().get(this.getPaddingSize() + 1);
    }

    default U1 getDefaultThird() {
        return this.getParameters().get(this.getPaddingSize() + 2);
    }

    default U1 getDefaultFourth() {
        return this.getParameters().get(this.getPaddingSize() + 3);
    }

    default U1 getLowFirst() {
        return this.getParameters().get(this.getPaddingSize() + 4);
    }

    default U1 getLowSecond() {
        return this.getParameters().get(this.getPaddingSize() + 5);
    }

    default U1 getLowThird() {
        return this.getParameters().get(this.getPaddingSize() + 6);
    }

    default U1 getLowFourth() {
        return this.getParameters().get(this.getPaddingSize() + 7);
    }

    default U1 getHighFirst() {
        return this.getParameters().get(this.getPaddingSize() + 8);
    }

    default U1 getHighSecond() {
        return this.getParameters().get(this.getPaddingSize() + 9);
    }

    default U1 getHighThird() {
        return this.getParameters().get(this.getPaddingSize() + 10);
    }

    default U1 getHighFourth() {
        return this.getParameters().get(this.getPaddingSize() + 11);
    }

    default int getDefault() {
        final int first = this.getDefaultFirst().byteValue();
        final int second = this.getDefaultSecond().intValue();
        final int third = this.getDefaultThird().intValue();
        final int fourth = this.getDefaultFourth().intValue();
        return (first << 24) | (second << 16) | (third << 8) | fourth;
    }

    default int getLow() {
        final int first = this.getLowFirst().byteValue();
        final int second = this.getLowSecond().intValue();
        final int third = this.getLowThird().intValue();
        final int fourth = this.getLowFourth().intValue();
        return (first << 24) | (second << 16) | (third << 8) | fourth;
    }

    default int getHigh() {
        final int first = this.getHighFirst().byteValue();
        final int second = this.getHighSecond().intValue();
        final int third = this.getHighThird().intValue();
        final int fourth = this.getHighFourth().intValue();
        return (first << 24) | (second << 16) | (third << 8) | fourth;
    }

    int getOffsetAt(final int position);

    @Override
    default byte getLength() {
        return (byte) (8 + this.getPaddingSize() + 4 * (this.getHigh() - this.getLow() + 1));
    }

    void setPaddingSize(final byte value);

    default void setDefaultFirst(final U1 value) {
        this.getParameters().set(this.getPaddingSize(), value);
    }

    default void setDefaultSecond(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 1, value);
    }

    default void setDefaultThird(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 2, value);
    }

    default void setDefaultFourth(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 3, value);
    }

    default void setLowFirst(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 4, value);
    }

    default void setLowSecond(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 5, value);
    }

    default void setLowThird(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 6, value);
    }

    default void setLowFourth(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 7, value);
    }

    default void setHighFirst(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 8, value);
    }

    default void setHighSecond(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 9, value);
    }

    default void setHighThird(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 10, value);
    }

    default void setHighFourth(final U1 value) {
        this.getParameters().set(this.getPaddingSize() + 11, value);
    }

    default void setDefault(final int value) {
        this.setDefaultFirst(U1.of((byte) (value >> 24)));
        this.setDefaultSecond(U1.of((byte) ((value >> 16) & 0xFF)));
        this.setDefaultThird(U1.of((byte) ((value >> 8) & 0xFF)));
        this.setDefaultFourth(U1.of((byte) (value & 0xFF)));
    }

    default void setLow(final int value) {
        this.setLowFirst(U1.of((byte) (value >> 24)));
        this.setLowSecond(U1.of((byte) ((value >> 16) & 0xFF)));
        this.setLowThird(U1.of((byte) ((value >> 8) & 0xFF)));
        this.setLowFourth(U1.of((byte) (value & 0xFF)));
    }

    default void setHigh(final int value) {
        this.setHighFirst(U1.of((byte) (value >> 24)));
        this.setHighSecond(U1.of((byte) ((value >> 16) & 0xFF)));
        this.setHighThird(U1.of((byte) ((value >> 8) & 0xFF)));
        this.setHighFourth(U1.of((byte) (value & 0xFF)));
    }

    void setOffsetAt(final int position, final int value);
}
