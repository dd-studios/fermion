package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.fermion.api.raw.verification.VerificationTypeInfo;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface SameLocals1StackItemFrame extends StackMapFrame {
    @Override
    default U2 getOffsetDelta() {
        return U2.of(this.getFrameType().intValue() - 64);
    }

    Table<VerificationTypeInfo> getStack();

    void setStack(final Table<VerificationTypeInfo> stack);
}
