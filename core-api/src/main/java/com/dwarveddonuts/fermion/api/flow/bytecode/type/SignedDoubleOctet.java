package com.dwarveddonuts.fermion.api.flow.bytecode.type;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class SignedDoubleOctet extends Number implements Comparable<SignedDoubleOctet> {
    private static final class Cache {
        private static final short CACHE_MIN = Byte.MIN_VALUE;
        private static final short CACHE_MAX = Byte.MAX_VALUE;
        private static final int CACHE_SIZE = -CACHE_MIN + CACHE_MAX + 1;

        private static final SignedDoubleOctet MIN = new SignedDoubleOctet(MIN_SIGNED_DOUBLE_OCTET);
        private static final SignedDoubleOctet MAX = new SignedDoubleOctet(MAX_SIGNED_DOUBLE_OCTET);
        private static final SignedDoubleOctet[] CACHE = new SignedDoubleOctet[CACHE_SIZE];

        static {
            IntStream.range(0, CACHE_SIZE).forEach(it -> CACHE[it] = new SignedDoubleOctet((short) (it + CACHE_MIN)));
        }
    }

    @Native public static final short MAX_SIGNED_DOUBLE_OCTET = Short.MAX_VALUE;
    @Native public static final short MIN_SIGNED_DOUBLE_OCTET = Short.MIN_VALUE;

    public static final int SIZE = Short.SIZE;
    public static final int BYTES = Short.BYTES;

    private final short value;

    private SignedDoubleOctet(final short value) {
        this.value = value;
    }

    public static SignedDoubleOctet of(final short value) {
        if (value == MIN_SIGNED_DOUBLE_OCTET) return Cache.MIN;
        if (value == MAX_SIGNED_DOUBLE_OCTET) return Cache.MAX;
        if (value < Cache.CACHE_MIN) return new SignedDoubleOctet(value);
        if (value >= Cache.CACHE_MAX) return new SignedDoubleOctet(value);
        return Cache.CACHE[value - Cache.CACHE_MIN];
    }

    public static SignedDoubleOctet of(final int value) {
        if (value < MIN_SIGNED_DOUBLE_OCTET || value > MAX_SIGNED_DOUBLE_OCTET) {
            throw new IllegalArgumentException(value + "is outside bounds for signed double octet: [" +
                    MIN_SIGNED_DOUBLE_OCTET + ", " + MAX_SIGNED_DOUBLE_OCTET + "]");
        }
        return of((short) value);
    }

    public SignedDoubleOctet add(final SignedDoubleOctet other) {
        return of((short) (this.value + other.value));
    }

    public SignedDoubleOctet subtract(final SignedDoubleOctet other) {
        return of((short) (this.value - other.value));
    }

    public SignedDoubleOctet multiply(final SignedDoubleOctet other) {
        return of((short) (this.value * other.value));
    }

    public SignedDoubleOctet divide(final SignedDoubleOctet other) {
        return of((short) (this.longValue() / other.longValue()));
    }

    @Override
    public int compareTo(@NotNull final SignedDoubleOctet that) {
        return Short.compare(this.value, that.value);
    }

    @Override
    public byte byteValue() {
        return (byte) this.value;
    }

    @Override
    public short shortValue() {
        return this.value;
    }

    @Override
    public int intValue() {
        return this.value;
    }

    @Override
    public long longValue() {
        return this.value;
    }

    @Override
    public float floatValue() {
        return this.value;
    }

    @Override
    public double doubleValue() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return Short.hashCode(this.value);
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof SignedDoubleOctet && this.value == ((SignedDoubleOctet) obj).value);
    }

    @Override
    public String toString() {
        return Short.toString(this.value);
    }
}
