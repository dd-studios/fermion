package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

public interface ElementValue extends ClassFileSpecPart {
    U1 BYTE = U1.of('B');
    U1 CHAR = U1.of('C');
    U1 DOUBLE = U1.of('D');
    U1 FLOAT = U1.of('F');
    U1 INT = U1.of('I');
    U1 LONG = U1.of('J');
    U1 SHORT = U1.of('S');
    U1 BOOLEAN = U1.of('Z');
    U1 STRING = U1.of('s');
    U1 ENUM = U1.of('e');
    U1 CLASS = U1.of('c');
    U1 ANNOTATION = U1.of('@');
    U1 ARRAY = U1.of('[');

    U1 getTag();
    Array<U1> getValue();

    void setTag(final U1 tag);
    void setValue(final Array<U1> value);
}
