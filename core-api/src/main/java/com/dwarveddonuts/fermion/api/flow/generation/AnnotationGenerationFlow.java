package com.dwarveddonuts.fermion.api.flow.generation;

import com.dwarveddonuts.fermion.api.description.ClassDescriptor;
import com.dwarveddonuts.fermion.api.description.InterfaceDescriptor;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public interface AnnotationGenerationFlow {
    interface Usage {
        void addElement(final String name, final boolean value);
        void addElement(final String name, final byte value);
        void addElement(final String name, final char value);
        void addElement(final String name, final double value);
        void addElement(final String name, final float value);
        void addElement(final String name, final int value);
        void addElement(final String name, final long value);
        void addElement(final String name, final short value);
        void addElement(final String name, final String value);
        <E extends Enum<E>> void addElement(final String name, final E value);
        void addElement(final String name, final ClassDescriptor clazz);
        void addElement(final String name, final InterfaceDescriptor annotation,
                        final Consumer<AnnotationGenerationFlow.Usage> flow);

        // TODO("Vararg?")
        void addElement(final String name, final boolean[] value);
        void addElement(final String name, final byte[] value);
        void addElement(final String name, final char[] value);
        void addElement(final String name, final double[] value);
        void addElement(final String name, final float[] value);
        void addElement(final String name, final int[] value);
        void addElement(final String name, final long[] value);
        void addElement(final String name, final short[] value);
        void addElement(final String name, final String[] value);
        <E extends Enum<E>> void addElement(final String name, final E[] value);
        void addElement(final String name, final ClassDescriptor[] classes);
        void addElement(final String name,
                        final Map<InterfaceDescriptor, Consumer<AnnotationGenerationFlow.Usage>> annotationsAndFlows);

        default void addElement(final String name, final Class<?> clazz) {
            this.addElement(name, ClassDescriptor.of(clazz));
        }

        default void addElement(final String name, final InterfaceDescriptor annotation) {
            this.addElement(name, annotation, flow -> {});
        }

        default void addElement(final String name, final Class<?>[] classes) {
            this.addElement(name, Arrays.stream(classes).map(ClassDescriptor::of).toArray(ClassDescriptor[]::new));
        }

        default void addElement(final String name, final InterfaceDescriptor[] annotations) {
            this.addElement(name, Arrays.stream(annotations).collect(Collectors.toMap(it -> it, it -> flow -> {})));
        }
    }
}
