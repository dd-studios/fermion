package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;

public interface BootstrapMethodsAttribute extends AttributeInfo {
    interface BootstrapMethodEntry {
        U2 getBootstrapMethodRef();
        U2 getNumBootstrapArguments();
        Array<U2> getBootstrapArguments();

        void setBoostrapMethodRef(final U2 boostrapMethodRef);
        void setNumBoostrapArguments(final U2 numBoostrapArguments);
        void setBootstrapArguments(final Array<U2> bootstrapArguments);
    }

    String ATTRIBUTE_NAME = "BootstrapMethods";

    U2 getNumBootstrapMethods();
    Table<BootstrapMethodEntry> getBootstrapMethods();

    void setNumBootstrapMethods(final U2 numBootstrapMethods);
    void setBootstrapMethods(final Table<BootstrapMethodEntry> bootstrapMethods);
}
