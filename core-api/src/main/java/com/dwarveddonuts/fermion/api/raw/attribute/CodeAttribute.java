package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;
import com.dwarveddonuts.vmtypes.U4;

public interface CodeAttribute extends AttributeInfo {
    interface ExceptionTableElement {
        U2 getStartPc();
        U2 getEndPc();
        U2 getHandlerPc();
        U2 getCatchType();

        void setStartPc(final U2 startPc);
        void setEndPc(final U2 endPc);
        void setHandlerPc(final U2 handlerPc);
        void setCatchType(final U2 catchType);
    }

    String ATTRIBUTE_NAME = "Code";

    U2 getMaxStack();
    U2 getMaxLocals();
    U4 getCodeLength();
    Array<U1> getCode();
    U2 getExceptionTableLength();
    Array<ExceptionTableElement> getExceptionTable();
    U2 getAttributesCount();
    Table<AttributeInfo> getAttributes();

    void setMaxStack(final U2 maxStack);
    void setMaxLocals(final U2 maxLocals);
    void setCodeLength(final U4 codeLength);
    void setCode(final Array<U1> code);
    void setExceptionTableLength(final U2 exceptionTableLength);
    void setExceptionTable(final Array<ExceptionTableElement> exceptionTable);
    void setAttributesCount(final U2 attributesCount);
    void setAttributes(final Table<AttributeInfo> attributes);
}
