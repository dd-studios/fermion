package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface SiPush extends CodeInstruction {
    U1 OPCODE = SIPUSH;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getImmediateTop() {
        return this.getParameters().get(0);
    }

    default U1 getImmediateBottom() {
        return this.getParameters().get(1);
    }

    default short getImmediate() {
        return (short) (((int) this.getImmediateTop().byteValue() << 8) | this.getImmediateBottom().intValue());
    }

    @Override
    default byte getLength() {
        return 1;
    }

    default void setImmediateTop(final U1 value) {
        this.getParameters().set(0, value);
    }

    default void setImmediateBottom(final U1 value) {
        this.getParameters().set(1, value);
    }

    default void setImmediate(final short value) {
        this.setImmediateTop(U1.of((byte) (value >> 8)));
        this.setImmediateBottom(U1.of((byte) (value & 0xFF)));
    }

    default void setImmediate(final int value) {
        this.setImmediate((short) value);
    }
}
