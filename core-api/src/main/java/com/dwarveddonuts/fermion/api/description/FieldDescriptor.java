package com.dwarveddonuts.fermion.api.description;

import java.util.Objects;

public final class FieldDescriptor {
    public static final class Builder {
        private final String name;
        private ComponentDescriptor type;

        private Builder(final String name) {
            this.name = name;
        }

        public Builder type(final ClassDescriptor fieldType) {
            this.type = ComponentDescriptor.ofClass(fieldType);
            return this;
        }

        public Builder type(final GenericDescriptor fieldType) {
            this.type = ComponentDescriptor.ofGeneric(fieldType);
            return this;
        }

        public FieldDescriptor make() {
            checkFieldName(this.name);
            if (this.type.isPureGeneric()) {
                checkGenericType(this.type.getPureGeneric());
            }
            // TODO("Additional checks")

            return new FieldDescriptor(this.name, this.type);
        }

        private static void checkFieldName(final String name) {
            // TODO("Check for invalid field names")
        }

        private static void checkGenericType(final GenericDescriptor generic) {
            // TODO("Generic type checks")
        }
    }

    private final String fieldName;
    private final ComponentDescriptor typeDescriptor;

    private FieldDescriptor(final String fieldName, final ComponentDescriptor typeDescriptor) {
        this.fieldName = fieldName;
        this.typeDescriptor = typeDescriptor;
    }

    public static FieldDescriptor of(final String fieldName, final ClassDescriptor fieldType) {
        Objects.requireNonNull(fieldName, "Unable to create a field descriptor with a null name");
        Objects.requireNonNull(fieldType, "Unable to create field descriptor with no class parameter");
        return builder(fieldName).type(fieldType).make();
    }

    public static FieldDescriptor of(final String fieldName, final GenericDescriptor fieldType) {
        Objects.requireNonNull(fieldName, "Unable to create a field descriptor with a null name");
        Objects.requireNonNull(fieldType, "Unable to create field descriptor with no generic data");
        return builder(fieldName).type(fieldType).make();
    }

    public static Builder builder(final String fieldName) {
        return new Builder(fieldName);
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public ComponentDescriptor getFieldType() {
        return this.typeDescriptor;
    }

    public String getErasedBytecodeName() {
        return this.getFieldType().getErasedBytecodeName();
    }

    public String getBytecodeName() {
        return this.getFieldType().getBytecodeName();
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.getBytecodeName(), this.fieldName);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final FieldDescriptor that = (FieldDescriptor) o;
        return this.fieldName.equals(that.fieldName) && this.typeDescriptor.equals(that.typeDescriptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.fieldName, this.typeDescriptor);
    }
}
