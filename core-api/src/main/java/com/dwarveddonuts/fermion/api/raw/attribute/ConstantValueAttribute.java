package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.U2;
import com.dwarveddonuts.vmtypes.U4;

public interface ConstantValueAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "ConstantValue";
    U4 LENGTH = U4.of(2);

    @Override
    default U4 getAttributeLength() {
        return LENGTH;
    }

    U2 getConstantValueIndex();

    @Override
    default void setAttributeLength(final U4 attributeLength) {
        if (!LENGTH.equals(attributeLength)) {
            throw new IllegalArgumentException("Invalid length for " + ATTRIBUTE_NAME + "Attribute");
        }
    }

    void setConstantValueIndex(final U2 constantValueIndex);
}
