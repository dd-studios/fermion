package com.dwarveddonuts.fermion.api.description;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public final class ClassDescriptor {
    public static final class Builder {
        private final String className;
        private boolean isArray;
        private int arrayDimensions;
        private final List<GenericDescriptor> generics;

        private Builder(final String className) {
            this.className = className;
            this.isArray = false;
            this.arrayDimensions = 0;
            this.generics = new ArrayList<>();
        }

        public Builder ofArray() {
            return this.ofArray(1);
        }

        public Builder ofArray(final int dimensions) {
            this.isArray = true;
            this.arrayDimensions = dimensions;
            return this;
        }

        public Builder withTypeParameter(final GenericDescriptor descriptor) {
            this.generics.add(descriptor);
            return this;
        }

        public ClassDescriptor make() {
            checkClassName(this.className);
            if (this.isArray) {
                checkArray(this.arrayDimensions);
            }
            checkGenerics(this.generics);
            final String bytecodeName = 'L' + this.className.replace('.', '/') + ';';

            return new ClassDescriptor(bytecodeName, this.arrayDimensions, new ArrayList<>(this.generics));
        }

        private static void checkClassName(final String className) {
            Objects.requireNonNull(className, "Unable to create class descriptor for a null name");
            if (className.contains("/")) {
                throw new IllegalArgumentException("Unable to create class descriptor for an invalid name: " +
                                                           "class name '" + className + "' must follow Java " +
                                                           "standards, with dots instead of slashes");
            }
            if (className.endsWith(";")) {
                throw new IllegalArgumentException("Unable to create class descriptor for an invalid name: " +
                                                           "invalid character ';' found in " + className);
            }
        }

        private static void checkArray(final int arrayDimensions) {
            if (arrayDimensions < 0) {
                throw new IllegalArgumentException("Unable to create array descriptor of a negative dimension");
            }
            if (arrayDimensions == 0) {
                throw new IllegalArgumentException("Unable to create array descriptor for a non-array");
            }
            if (arrayDimensions > 255) {
                throw new IllegalArgumentException("Unable to create array descriptor of more than 255 dimensions");
            }
        }

        private static void checkGenerics(final List<GenericDescriptor> descriptors) {
            descriptors.forEach(Builder::checkGeneric);
        }

        private static void checkGeneric(final GenericDescriptor descriptor) {
            // TODO("");
        }
    }

    private static final Set<ClassDescriptor> PRIMITIVES = new HashSet<>();

    public static final ClassDescriptor BOOLEAN = ofPrimitive("Z");
    public static final ClassDescriptor BYTE = ofPrimitive("B");
    public static final ClassDescriptor CHAR = ofPrimitive("C");
    public static final ClassDescriptor DOUBLE = ofPrimitive("D");
    public static final ClassDescriptor FLOAT = ofPrimitive("F");
    public static final ClassDescriptor INT = ofPrimitive("I");
    public static final ClassDescriptor LONG = ofPrimitive("J");
    public static final ClassDescriptor SHORT = ofPrimitive("S");
    public static final ClassDescriptor VOID = ofPrimitive("V");

    public static final ClassDescriptor OBJECT = of("java.lang.Object");

    private final String baseBytecodeName;
    private final int arrayDimensions;
    private final List<GenericDescriptor> genericParameters;

    private ClassDescriptor(final String baseBytecodeName, final int arrayDimensions,
                            final List<GenericDescriptor> genericParameters) {
        this.baseBytecodeName = baseBytecodeName;
        this.arrayDimensions = arrayDimensions;
        this.genericParameters = genericParameters;
    }

    private ClassDescriptor(final String baseBytecodeName) {
        this(baseBytecodeName, 0, Collections.emptyList());
    }

    private static ClassDescriptor ofPrimitive(final String id) {
        final var desc = new ClassDescriptor(Objects.requireNonNull(id, "id"));
        PRIMITIVES.add(desc);
        return desc;
    }

    public static ClassDescriptor of(final Class<?> clazz) {
        Objects.requireNonNull(clazz, "Unable to create class descriptor for a null class");
        if (clazz.isInterface()) {
            throw new IllegalArgumentException( "Unable to create class descriptor for interface class");
        }

        if (clazz == boolean.class) return BOOLEAN;
        if (clazz == byte.class) return BYTE;
        if (clazz == char.class) return CHAR;
        if (clazz == double.class) return DOUBLE;
        if (clazz == float.class) return FLOAT;
        if (clazz == int.class) return INT;
        if (clazz == long.class) return LONG;
        if (clazz == short.class) return SHORT;
        if (clazz == void.class) return VOID;
        if (clazz == Object.class) return OBJECT;

        if (clazz.isArray()) {
            return ofArray(of(clazz.getComponentType()), 1);
        }

        // TODO("type parameters (needs GenericDescriptor to be further expanded)")
        return of(clazz.getName());
    }

    public static ClassDescriptor of(final String className) {
        return builderFor(className).make();
    }

    public static ClassDescriptor ofArray(final Class<?> elementClass, final int arrayDimensions) {
        return ofArray(of(elementClass), arrayDimensions);
    }

    public static ClassDescriptor ofArray(final ClassDescriptor descriptor, final int arrayDimensions) {
        Objects.requireNonNull(descriptor, "Unable to create array descriptor of element type null");
        if (descriptor == VOID) {
            throw new IllegalArgumentException("Unable to create array descriptor of element type 'void'");
        }
        return ofArray(descriptor.getClassName(), descriptor.getArrayDimensions() + arrayDimensions);
    }

    public static ClassDescriptor ofArray(final String className, final int arrayDimensions) {
        return builderFor(className).ofArray(arrayDimensions).make();
    }

    public static ClassDescriptor.Builder builderFor(final String className) {
        return new Builder(className);
    }

    public String getClassName() {
        return this.isPrimitive()?
                this.baseBytecodeName :
                this.baseBytecodeName.substring(1, this.baseBytecodeName.length() - 1).replace('/', '.');
    }

    public String getErasedBytecodeName() {
        return "[".repeat(Math.max(0, this.arrayDimensions)) + this.baseBytecodeName;
    }

    public String getBytecodeName() {
        final var erasedBytecodeName = this.getErasedBytecodeName();

        if (!this.isGenericType()) {
            return erasedBytecodeName;
        }

        return this.genericParameters.stream()
                .map(GenericDescriptor::toString)
                .collect(Collectors.joining("", erasedBytecodeName + '<', ">"));
    }

    public int getArrayDimensions() {
        return this.arrayDimensions;
    }

    public boolean isArray() {
        return this.arrayDimensions != 0;
    }

    public boolean isPrimitive() {
        return PRIMITIVES.contains(this);
    }

    public boolean isGenericType() {
        return !this.genericParameters.isEmpty();
    }

    public List<GenericDescriptor> getTypeParameters() {
        if (!this.isGenericType()) {
            throw new IllegalStateException("This descriptor does not describe a generic type");
        }
        return Collections.unmodifiableList(this.genericParameters);
    }

    @Override
    public String toString() {
        return this.getBytecodeName();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (ClassDescriptor) o;
        return this.arrayDimensions == that.arrayDimensions && this.baseBytecodeName.equals(that.baseBytecodeName)
                && this.genericParameters.equals(that.genericParameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.baseBytecodeName, this.arrayDimensions, this.genericParameters);
    }
}
