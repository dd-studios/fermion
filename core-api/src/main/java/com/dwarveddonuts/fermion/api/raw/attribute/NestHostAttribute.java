package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.U2;
import com.dwarveddonuts.vmtypes.U4;

public interface NestHostAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "NestHost";
    U4 LENGTH = U4.of(2);

    @Override
    default U4 getAttributeLength() {
        return LENGTH;
    }

    U2 getHostClassIndex();

    @Override
    default void setAttributeLength(final U4 attributeLength) {
        if (!LENGTH.equals(attributeLength)) {
            throw new IllegalArgumentException("Invalid length for " + ATTRIBUTE_NAME + "Attribute");
        }
    }

    void setHostClassIndex(final U2 hostClassIndex);
}
