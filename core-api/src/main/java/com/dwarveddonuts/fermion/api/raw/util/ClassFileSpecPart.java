package com.dwarveddonuts.fermion.api.raw.util;

import java.nio.ByteBuffer;

public interface ClassFileSpecPart {
    ByteBuffer write(final ByteBuffer buffer);
}
