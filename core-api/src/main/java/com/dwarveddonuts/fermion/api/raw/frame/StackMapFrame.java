package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface StackMapFrame extends ClassFileSpecPart {
    Collection<U1> SAME = Collections.unmodifiableList(
            IntStream.rangeClosed(0, 63).mapToObj(U1::of).collect(Collectors.toList())
    );
    Collection<U1> SAME_LOCALS_1_STACK_ITEM = Collections.unmodifiableList(
            IntStream.rangeClosed(64, 127).mapToObj(U1::of).collect(Collectors.toList())
    );
    U1 SAME_LOCALS_1_STACK_ITEM_EXTENDED = U1.of(247);
    Collection<U1> CHOP = Collections.unmodifiableList(
            IntStream.rangeClosed(248, 250).mapToObj(U1::of).collect(Collectors.toList())
    );
    U1 SAME_FRAME_EXTENDED = U1.of(251);
    Collection<U1> APPEND = Collections.unmodifiableList(
            IntStream.rangeClosed(252, 254).mapToObj(U1::of).collect(Collectors.toList())
    );
    U1 FULL_FRAME = U1.of(255);

    U1 getFrameType();
    U2 getOffsetDelta();

    void setFrameType(final U1 frameType);
    void setOffsetDelta(final U2 offsetDelta);
}
