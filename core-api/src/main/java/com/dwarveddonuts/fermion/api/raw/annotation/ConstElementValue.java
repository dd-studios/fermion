package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.vmtypes.U2;

public interface ConstElementValue extends ElementValue {
    U2 getConstValueIndex();

    void setConstValueIndex(final U2 constValueIndex);
}
