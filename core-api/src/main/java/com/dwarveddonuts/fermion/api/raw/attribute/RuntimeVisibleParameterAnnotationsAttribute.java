package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.annotation.Annotation;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface RuntimeVisibleParameterAnnotationsAttribute extends AttributeInfo {
    interface ParameterAnnotationElement {
        U2 getNumAnnotations();
        Table<Annotation> getAnnotations();

        void setNumAnnotations(final U2 numAnnotations);
        void setAnnotations(final Table<Annotation> annotations);
    }

    String ATTRIBUTE_NAME = "RuntimeVisibleParameterAnnotations";

    U1 getNumParameters();
    Table<ParameterAnnotationElement> getParameterAnnotations();

    void setNumParameters(final U1 numParameters);
    void setParameterAnnotations(final Table<ParameterAnnotationElement> annotations);
}
