package com.dwarveddonuts.fermion.api.raw;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.fermion.api.raw.pool.CpInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U2;
import com.dwarveddonuts.vmtypes.U4;

public interface ClassFile extends ClassFileSpecPart {
    U4 getMagic();
    U2 getMinorVersion();
    U2 getMajorVersion();
    U2 getConstantPoolCount();
    Table<CpInfo> getConstantPool();
    U2 getAccessFlags();
    U2 getThisClass();
    U2 getSuperClass();
    U2 getInterfacesCount();
    Array<U2> getInterfaces();
    U2 getFieldsCount();
    Table<FieldInfo> getFields();
    U2 getMethodsCount();
    Table<MethodInfo> getMethods();
    U2 getAttributesCount();
    Table<AttributeInfo> getAttributes();

    void setMagic(final U4 magic);
    void setMinorVersion(final U2 minorVersion);
    void setMajorVersion(final U2 majorVersion);
    void setConstantPoolCount(final U2 constantPoolCount);
    void setConstantPool(final Table<CpInfo> constantPool);
    void setAccessFlags(final U2 accessFlags);
    void setThisClass(final U2 thisClass);
    void setSuperClass(final U2 superClass);
    void setInterfacesCount(final U2 interfacesCount);
    void setInterfaces(final Array<U2> interfaces);
    void setFieldsCount(final U2 fieldsCount);
    void setFields(final Table<FieldInfo> fields);
    void setMethodsCount(final U2 methodsCount);
    void setMethods(final Table<MethodInfo> methods);
    void setAttributesCount(final U2 attributesCount);
    void setAttributes(final Table<AttributeInfo> attributes);

    byte[] serialize();
}
