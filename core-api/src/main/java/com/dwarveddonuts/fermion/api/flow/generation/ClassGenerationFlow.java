package com.dwarveddonuts.fermion.api.flow.generation;

import com.dwarveddonuts.fermion.api.description.FieldDescriptor;
import com.dwarveddonuts.fermion.api.description.InterfaceDescriptor;
import com.dwarveddonuts.fermion.api.description.MethodDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.FieldPrototype;
import com.dwarveddonuts.fermion.api.description.prototype.MethodPrototype;
import com.dwarveddonuts.fermion.api.flow.ClassResult;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface ClassGenerationFlow {
    void annotate(final InterfaceDescriptor annotation, final Consumer<AnnotationGenerationFlow.Usage> flow);

    MethodDescriptor prototypeMethod(final MethodPrototype prototype);
    MethodDescriptor generateMethod(final MethodDescriptor descriptor,
                                    final BiConsumer<MethodDescriptor, MethodGenerationFlow> flow);

    FieldDescriptor prototypeField(final FieldPrototype prototype);
    FieldDescriptor generateField(final FieldDescriptor descriptor,
                                  final BiConsumer<FieldDescriptor, FieldGenerationFlow> flow);

    ClassResult end();

    default void annotate(final InterfaceDescriptor annotation) {
        this.annotate(annotation, flow -> {});
    }

    default MethodDescriptor generateMethod(final MethodPrototype prototype,
                                            final BiConsumer<MethodDescriptor, MethodGenerationFlow> flow) {
        return this.generateMethod(this.prototypeMethod(prototype), flow);
    }

    default FieldDescriptor generateField(final FieldPrototype prototype,
                                          final BiConsumer<FieldDescriptor, FieldGenerationFlow> flow) {
        return this.generateField(this.prototypeField(prototype), flow);
    }

    default MethodDescriptor generateSimpleMethod(final MethodDescriptor descriptor,
                                                  final BiConsumer<MethodDescriptor, MethodGenerationFlow> flow) {
        return this.generateMethod(MethodPrototype.simple(descriptor), flow);
    }

    default FieldDescriptor generateSimpleField(final FieldDescriptor descriptor,
                                                final BiConsumer<FieldDescriptor, FieldGenerationFlow> flow) {
        return this.generateField(FieldPrototype.simple(descriptor), flow);
    }
}
