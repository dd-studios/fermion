package com.dwarveddonuts.fermion.api.flow.bytecode.type;

import com.dwarveddonuts.vmtypes.U1;
import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class UnsignedOctet extends Number implements Comparable<UnsignedOctet> {
    private static final class Cache {
        private static final UnsignedOctet[] CACHE = new UnsignedOctet[MAX_UNSIGNED_OCTET + 1];

        static {
            IntStream.rangeClosed(MIN_UNSIGNED_OCTET, MAX_UNSIGNED_OCTET)
                    .forEach(it -> CACHE[it - MIN_UNSIGNED_OCTET] = new UnsignedOctet(U1.of(it)));
        }
    }

    @Native public static final short MAX_UNSIGNED_OCTET = U1.MAX_U1;
    @Native public static final short MIN_UNSIGNED_OCTET = U1.MIN_U1;

    public static final int SIZE = U1.SIZE;
    public static final int BYTES = U1.BYTES;

    private final U1 value;

    private UnsignedOctet(final U1 value) {
        this.value = value;
    }

    public static UnsignedOctet of(final U1 value) {
        return Cache.CACHE[value.intValue() - MIN_UNSIGNED_OCTET];
    }

    public static UnsignedOctet of(final byte value) {
        return of(U1.of(value));
    }

    public static UnsignedOctet of(final int value) {
        if (value < MIN_UNSIGNED_OCTET || value > MAX_UNSIGNED_OCTET) {
            throw new IllegalArgumentException(value + "is outside bounds for unsigned octet: [" +
                    MIN_UNSIGNED_OCTET + ", " + MAX_UNSIGNED_OCTET + "]");
        }
        return of((byte) value);
    }

    public UnsignedOctet add(final UnsignedOctet other) {
        return of(this.value.add(other.value));
    }

    public UnsignedOctet subtract(final UnsignedOctet other) {
        return of(this.value.subtract(other.value));
    }

    public UnsignedOctet multiply(final UnsignedOctet other) {
        return of(this.value.multiply(other.value));
    }

    public UnsignedOctet divide(final UnsignedOctet other) {
        return of(this.value.divide(other.value));
    }

    @Override
    public int compareTo(@NotNull final UnsignedOctet that) {
        return this.value.compareTo(that.value);
    }

    @Override
    public byte byteValue() {
        return this.value.byteValue();
    }

    @Override
    public short shortValue() {
        return this.value.shortValue();
    }

    @Override
    public int intValue() {
        return this.value.intValue();
    }

    @Override
    public long longValue() {
        return this.value.longValue();
    }

    @Override
    public float floatValue() {
        return this.value.floatValue();
    }

    @Override
    public double doubleValue() {
        return this.value.doubleValue();
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof UnsignedOctet && this.value.equals(((UnsignedOctet) obj).value));
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
