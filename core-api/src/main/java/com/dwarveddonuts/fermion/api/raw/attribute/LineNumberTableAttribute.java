package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface LineNumberTableAttribute extends AttributeInfo {
    interface LineNumberTableElement {
        U2 getStartPc();
        U2 getLineNumber();

        void setStartPc(final U2 startPc);
        void setLineNumber(final U2 lineNumber);
    }

    String ATTRIBUTE_NAME = "LineNumberTable";

    U2 getLineNumberTableLength();
    Array<U2> getLineNumber();

    void setLineNumberTableLength(final U2 lineNumberTableLength);
    void setLineNumber(final Array<U2> lineNumber);
}
