package com.dwarveddonuts.fermion.api.flow;

import com.dwarveddonuts.fermion.api.lens.ClassLens;
import com.dwarveddonuts.fermion.api.raw.ClassFile;

public interface ClassResult {
    ClassLens asLens();
    ClassFile asRaw();

    default byte[] serialize() {
        return this.asRaw().serialize();
    }
}
