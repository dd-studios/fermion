package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface BiPush extends CodeInstruction {
    U1 OPCODE = BIPUSH;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default byte getImmediate() {
        return this.getParameters().get(0).byteValue();
    }

    @Override
    default byte getLength() {
        return 1;
    }

    default void setImmediate(final byte value) {
        this.getParameters().set(0, U1.of(value));
    }
}
