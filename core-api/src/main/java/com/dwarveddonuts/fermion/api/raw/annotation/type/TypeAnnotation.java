package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.fermion.api.raw.annotation.ElementValue;
import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.Table;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface TypeAnnotation extends ClassFileSpecPart {
    interface ElementValuePair {
        U2 getElementNameIndex();
        ElementValue getValue();

        void setElementNameIndex(final U2 elementNameIndex);
        void setValue(final ElementValue value);
    }

    U1 getTargetType();
    Array<U1> getTargetInfo();
    TypePath getTargetPath();
    U2 getTypeIndex();
    U2 getNumElementValuePairs();
    Table<ElementValuePair> getElementValuePairs();

    void setTargetType(final U1 targetType);
    void setTargetInfo(final Array<U1> targetInfo);
    void setTargetPath(final TypePath targetPath);
    void setTypeIndex(final U2 typeIndex);
    void setNumElementValuePairs(final U2 numElementValuePairs);
    void setElementValuePairs(final Table<ElementValuePair> elementValuePairs);
}
