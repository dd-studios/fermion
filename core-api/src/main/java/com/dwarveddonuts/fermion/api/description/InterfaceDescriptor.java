package com.dwarveddonuts.fermion.api.description;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class InterfaceDescriptor {
    public static final class Builder {
        private final String interfaceName;
        private final List<GenericDescriptor> generics;

        private Builder(final String interfaceName) {
            this.interfaceName = interfaceName;
            this.generics = new ArrayList<>();
        }

        public Builder withTypeParameter(final GenericDescriptor descriptor) {
            this.generics.add(descriptor);
            return this;
        }

        public InterfaceDescriptor make() {
            checkInterfaceName(this.interfaceName);
            checkGenerics(this.generics);
            final String bytecodeName = 'L' + this.interfaceName.replace('.', '/') + ';';

            return new InterfaceDescriptor(bytecodeName, new ArrayList<>(this.generics));
        }

        private static void checkInterfaceName(final String interfaceName) {
            Objects.requireNonNull(interfaceName, "Unable to create interface descriptor for a null name");
            if (interfaceName.contains("/")) {
                throw new IllegalArgumentException("Unable to create interface descriptor for an invalid name: " +
                        "interface name '" + interfaceName + "' must follow Java " +
                        "standards, with dots instead of slashes");
            }
            if (interfaceName.endsWith(";")) {
                throw new IllegalArgumentException("Unable to create interface descriptor for an invalid name: " +
                        "invalid character ';' found in " + interfaceName);
            }
        }

        private static void checkGenerics(final List<GenericDescriptor> descriptors) {
            descriptors.forEach(Builder::checkGeneric);
        }

        private static void checkGeneric(final GenericDescriptor descriptor) {
            // TODO("");
        }
    }

    private final String baseBytecodeName;
    private final List<GenericDescriptor> genericParameters;

    private InterfaceDescriptor(final String baseBytecodeName, final List<GenericDescriptor> genericParameters) {
        this.baseBytecodeName = baseBytecodeName;
        this.genericParameters = genericParameters;
    }

    public static InterfaceDescriptor of(final Class<?> clazz) {
        Objects.requireNonNull(clazz, "Unable to create interface descriptor for a null class");
        if (!clazz.isInterface()) {
            throw new IllegalArgumentException("Unable to create interface descriptor for non-interface class");
        }

        // TODO("type parameters (needs GenericDescriptor to be further expanded)")
        return of(clazz.getName());
    }

    public static InterfaceDescriptor of(final String interfaceName) {
        return builderFor(interfaceName).make();
    }

    public static Builder builderFor(final String interfaceName) {
        return new Builder(interfaceName);
    }

    public String getInterfaceName() {
        return this.baseBytecodeName.substring(1, this.baseBytecodeName.length() - 1)
                .replace('/', '.');
    }

    public String getErasedBytecodeName() {
        return this.baseBytecodeName;
    }

    public String getBytecodeName() {
        final var erasedBytecodeName = this.getErasedBytecodeName();

        if (!this.isGenericType()) {
            return erasedBytecodeName;
        }

        return this.genericParameters.stream()
                .map(GenericDescriptor::toString)
                .collect(Collectors.joining("", erasedBytecodeName + '<', ">"));
    }

    public boolean isGenericType() {
        return !this.genericParameters.isEmpty();
    }

    public List<GenericDescriptor> getTypeParameters() {
        if (!this.isGenericType()) {
            throw new IllegalStateException("This descriptor does not describe a generic type");
        }
        return Collections.unmodifiableList(this.genericParameters);
    }

    @Override
    public String toString() {
        return this.getBytecodeName();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (InterfaceDescriptor) o;
        return this.baseBytecodeName.equals(that.baseBytecodeName)
                && this.genericParameters.equals(that.genericParameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.baseBytecodeName, this.genericParameters);
    }
}
