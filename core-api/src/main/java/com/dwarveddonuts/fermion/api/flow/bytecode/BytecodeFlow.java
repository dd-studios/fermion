package com.dwarveddonuts.fermion.api.flow.bytecode;

import com.dwarveddonuts.fermion.api.description.ClassDescriptor;
import com.dwarveddonuts.fermion.api.description.FieldDescriptor;
import com.dwarveddonuts.fermion.api.description.InterfaceDescriptor;
import com.dwarveddonuts.fermion.api.description.MethodDescriptor;
import com.dwarveddonuts.fermion.api.flow.bytecode.type.SignedDoubleOctet;
import com.dwarveddonuts.fermion.api.flow.bytecode.type.SignedOctet;
import com.dwarveddonuts.fermion.api.flow.bytecode.type.UnsignedOctet;

import java.util.function.Consumer;

@SuppressWarnings("SpellCheckingInspection")
public interface BytecodeFlow {
    // TODO("Add arguments to functions that require them")
    Label _label(final int id);
    void _lineNumber(final Label target, final int lineNumber);

    void _tryCatch(final Label tryBegin, final Label tryEnd, final ClassDescriptor exceptionType, final Label handler);
    void _tryFinally(final Label tryBegin, final Label tryEnd, final Label handler);

    void _sameFrame();
    void _sameLocalsOneStackFrame(/**/);
    void _chopFrame(/**/);
    void _appendFrame(/**/);
    void _fullFrame(/**/);

    void _maxStack(/**/);
    void _maxLocals(/**/);

    void nop();
    void aconst_null();
    void iconst_m1();
    void iconst_0();
    void iconst_1();
    void iconst_2();
    void iconst_3();
    void iconst_4();
    void iconst_5();
    void lconst_0();
    void lconst_1();
    void fconst_0();
    void fconst_1();
    void fconst_2();
    void dconst_0();
    void dconst_1();
    void bipush(final SignedOctet value);
    void sipush(final SignedDoubleOctet value);
    void ldc(final int value); // CONSTANT_Integer
    void ldc(final float value); // CONSTANT_Float
    void ldc(final String value); // CONSTANT_String
    void ldc(final ClassDescriptor value); // CONSTANT_Class
    void ldc(final InterfaceDescriptor value); // CONSTANT_Class TODO("Check")
    void ldc(final MethodDescriptor value); // CONSTANT_MethodType
    // void ldc(final MethodHandle value); // CONSTANT_MethodHandle TODO()
    // void ldc(final DynamicConstant value); // CONSTANT_Dynamic TODO()
    void ldc_w(final int value); // CONSTANT_Integer
    void ldc_w(final float value); // CONSTANT_Float
    void ldc_w(final String value); // CONSTANT_String
    void ldc_w(final ClassDescriptor value); // CONSTANT_Class
    void ldc_w(final InterfaceDescriptor value); // CONSTANT_Class TODO("Check")
    void ldc_w(final MethodDescriptor value); // CONSTANT_MethodType
    // void ldc_w(final MethodHandle value); // CONSTANT_MethodHandle TODO()
    // void ldc_w(final DynamicConstant value); // CONSTANT_Dynamic TODO()
    void ldc2_w(final long value); // CONSTANT_Long
    void ldc2_w(final double value); // CONSTANT_Double
    // void lc2_w(final DynamicConstant value); // CONSTANT_Dynamic TODO()
    void iload(final UnsignedOctet localIndex);
    void lload(final UnsignedOctet localIndex);
    void fload(final UnsignedOctet localIndex);
    void dload(final UnsignedOctet localIndex);
    void aload(final UnsignedOctet localIndex);
    void iload_0();
    void iload_1();
    void iload_2();
    void iload_3();
    void lload_0();
    void lload_1();
    void lload_2();
    void lload_3();
    void fload_0();
    void fload_1();
    void fload_2();
    void fload_3();
    void dload_0();
    void dload_1();
    void dload_2();
    void dload_3();
    void aload_0();
    void aload_1();
    void aload_2();
    void aload_3();
    void iaload();
    void laload();
    void faload();
    void daload();
    void aaload();
    void baload();
    void caload();
    void saload();
    void istore(final UnsignedOctet localIndex);
    void lstore(final UnsignedOctet localIndex);
    void fstore(final UnsignedOctet localIndex);
    void dstore(final UnsignedOctet localIndex);
    void astore(final UnsignedOctet localIndex);
    void istore_0();
    void istore_1();
    void istore_2();
    void istore_3();
    void lstore_0();
    void lstore_1();
    void lstore_2();
    void lstore_3();
    void fstore_0();
    void fstore_1();
    void fstore_2();
    void fstore_3();
    void dstore_0();
    void dstore_1();
    void dstore_2();
    void dstore_3();
    void astore_0();
    void astore_1();
    void astore_2();
    void astore_3();
    void iastore();
    void lastore();
    void fastore();
    void dastore();
    void aastore();
    void bastore();
    void castore();
    void sastore();
    void pop();
    void pop2();
    void dup();
    void dup_x1();
    void dup_x2();
    void dup2();
    void dup2_x1();
    void dup2_x2();
    void swap();
    void iadd();
    void ladd();
    void fadd();
    void dadd();
    void isub();
    void lsub();
    void fsub();
    void dsub();
    void imul();
    void lmul();
    void fmul();
    void dmul();
    void idiv();
    void ldiv();
    void fdiv();
    void ddiv();
    void irem();
    void lrem();
    void frem();
    void drem();
    void ineg();
    void lneg();
    void fneg();
    void dneg();
    void ishl();
    void lshl();
    void ishr();
    void lshr();
    void iushr();
    void lushr();
    void iand();
    void land();
    void ior();
    void lor();
    void ixor();
    void lxor();
    void iinc(final UnsignedOctet localIndex, final SignedOctet constant);
    void i2l();
    void i2f();
    void i2d();
    void l2i();
    void l2f();
    void l2d();
    void f2i();
    void f2l();
    void f2d();
    void d2i();
    void d2l();
    void d2f();
    void i2b();
    void i2c();
    void i2s();
    void lcmp();
    void fcmpl();
    void fcmpg();
    void dcmpl();
    void dcmpg();
    void ifeq(final Label jumpTarget);
    void ifne(final Label jumpTarget);
    void iflt(final Label jumpTarget);
    void ifge(final Label jumpTarget);
    void ifgt(final Label jumpTarget);
    void ifle(final Label jumpTarget);
    void if_icmpeq(final Label jumpTarget);
    void if_icmpne(final Label jumpTarget);
    void if_icmplt(final Label jumpTarget);
    void if_icmpge(final Label jumpTarget);
    void if_icmpgt(final Label jumpTarget);
    void if_icmple(final Label jumpTarget);
    void if_acmpeq(final Label jumpTarget);
    void if_acmpne(final Label jumpTarget);
    void goto_(final Label jumpTarget);
    void jsr(final Label jumpTarget);
    void ret(final Label returnTarget);
    void tableswitch(final Label defaultTarget, final int low, final int high, final Label... jumpTargets);
    // void tableswitch(final Label defaultTarget, final SwitchCasePair cases); ? Maybe not?
    void lookupswitch(final Label defaultTarget, final SwitchCasePair cases);
    void ireturn();
    void lreturn();
    void freturn();
    void dreturn();
    void areturn();
    void return_();
    void getstatic(final ClassDescriptor owner, final FieldDescriptor target);
    void putstatic(final ClassDescriptor owner, final FieldDescriptor target);
    void getfield(final ClassDescriptor owner, final FieldDescriptor target);
    void putfield(final ClassDescriptor owner, final FieldDescriptor target);
    void invokevirtual(final ClassDescriptor owner, final MethodDescriptor target);
    void invokespecial(final ClassDescriptor owner, final MethodDescriptor target);
    void invokespecial(final InterfaceDescriptor owner, final MethodDescriptor target);
    void invokestatic(final ClassDescriptor owner, final MethodDescriptor target);
    void invokestatic(final InterfaceDescriptor owner, final MethodDescriptor target);
    void invokeinterface(final InterfaceDescriptor owner, final MethodDescriptor target);
    void invokedynamic(/**/); // TODO("MethodHandle? CallSite? Needs to be identified")
    void new_(final ClassDescriptor target);
    void newarray(final ArrayType type);
    void anewarray(final ClassDescriptor type); // Also interfaces and arrays
    void anewarray(final InterfaceDescriptor type);
    void arraylength();
    void athrow();
    void checkcast(final ClassDescriptor type); // Also interfaces and arrays
    void checkcast(final InterfaceDescriptor type);
    void instanceof_(final ClassDescriptor type); // Also interfaces and arrays
    void instanceof_(final InterfaceDescriptor type);
    void monitorenter();
    void monitorexit();
    void wide(final Consumer<WideBytecodeFlow> flow);
    void multianewarray(final ClassDescriptor type, final UnsignedOctet dimensions); // Also interfaces and arrays
    void multianewarray(final InterfaceDescriptor type, final UnsignedOctet dimensions);
    void ifnull(final Label jumpTarget);
    void ifnonnull(final Label jumpTarget);
    void goto_w(final Label jumpTarget);
    void jsr_w(final Label jumpTarget);

    default void breakpoint() {
        throw new UnsupportedOperationException("This bytecode instruction cannot appear in a valid class file");
    }

    default void impdep1() {
        throw new UnsupportedOperationException("This bytecode instruction cannot appear in a valid class file");
    }

    default void impdep2() {
        throw new UnsupportedOperationException("This bytecode instruction cannot appear in a valid class file");
    }
}
