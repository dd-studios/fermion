package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.vmtypes.U1;

public interface AnnotationValue extends ElementValue {
    @Override
    default U1 getTag() {
        return ANNOTATION;
    }

    Annotation getAnnotationValue();

    @Override
    default void setTag(final U1 tag) {
        if (!ANNOTATION.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for AnnotationValue");
        }
    }

    void setAnnotationValue(final Annotation annotationValue);
}
