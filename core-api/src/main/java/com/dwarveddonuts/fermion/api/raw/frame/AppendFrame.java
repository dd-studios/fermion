package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.fermion.api.raw.verification.VerificationTypeInfo;
import com.dwarveddonuts.vmtypes.Table;

public interface AppendFrame extends StackMapFrame {
    Table<VerificationTypeInfo> getLocals();

    void setLocals(final Table<VerificationTypeInfo> locals);
}
