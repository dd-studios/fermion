package com.dwarveddonuts.fermion.api.raw.annotation;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ClassInfoValue extends ElementValue {
    @Override
    default U1 getTag() {
        return CLASS;
    }

    U2 getClassInfoIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!CLASS.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ClassInfoValue");
        }
    }

    void setClassInfoIndex(final U2 classInfoIndex);
}
