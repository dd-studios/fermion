package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U2;

public interface OffsetTargetTypeAnnotation extends TypeAnnotation {
    U2 getOffset();

    void setOffset(final U2 offset);
}
