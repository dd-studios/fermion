package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface InvokeVirtual extends CodeInstruction {
    U1 OPCODE = INVOKEVIRTUAL;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getIndexTop() {
        return this.getParameters().get(0);
    }

    default U1 getIndexBottom() {
        return this.getParameters().get(1);
    }

    default U2 getIndex() {
        return U2.of(this.getIndexTop().intValue() << 8 | this.getIndexBottom().intValue());
    }

    @Override
    default byte getLength() {
        return 2;
    }

    default void setIndexTop(final U1 value) {
        this.getParameters().set(0, value);
    }

    default void setIndexBottom(final U1 value) {
        this.getParameters().set(1, value);
    }

    default void setIndex(final U2 value) {
        this.setIndexTop(U1.of(value.intValue() >> 8));
        this.setIndexBottom(U1.of(value.byteValue()));
    }
}
