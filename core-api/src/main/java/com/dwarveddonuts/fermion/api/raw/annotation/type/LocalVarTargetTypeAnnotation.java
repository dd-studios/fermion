package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface LocalVarTargetTypeAnnotation extends TypeAnnotation {
    interface TableEntry {
        U2 getStartPc();
        U2 getLength();
        U2 getIndex();

        void setStartPc(final U2 startPc);
        void setLength(final U2 length);
        void setIndex(final U2 index);
    }

    U2 getTableLength();
    Array<TableEntry> getTable();

    void setThrowsTypeIndex(final U2 throwsTypeIndex);
    void setTable(final Array<TableEntry> table);
}
