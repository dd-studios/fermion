package com.dwarveddonuts.fermion.api.flow.bytecode;

public interface Label {
    int getId();
}
