package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

import java.util.Arrays;
import java.util.List;

public interface NewArray extends CodeInstruction {
    U1 OPCODE = NEWARRAY;

    U1 T_BOOLEAN = U1.of(0x4);
    U1 T_CHAR = U1.of(0x5);
    U1 T_FLOAT = U1.of(0x6);
    U1 T_DOUBLE = U1.of(0x7);
    U1 T_BYTE = U1.of(0x8);
    U1 T_SHORT = U1.of(0x9);
    U1 T_INT = U1.of(0xA);
    U1 T_LONG = U1.of(0xB);

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getAType() {
        return this.getParameters().get(0);
    }

    @Override
    default byte getLength() {
        return 1;
    }

    default void setAType(final U1 value) {
        final List<U1> types = Arrays.asList(T_BOOLEAN, T_CHAR, T_FLOAT, T_DOUBLE, T_BYTE, T_SHORT, T_INT, T_LONG);
        if (!types.contains(value)) {
            throw new IllegalArgumentException("Invalid a-type given for 'newarray' instruction: " + value);
        }
        this.getParameters().set(0, value);
    }
}
