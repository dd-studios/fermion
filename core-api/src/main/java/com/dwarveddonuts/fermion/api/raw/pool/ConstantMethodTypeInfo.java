package com.dwarveddonuts.fermion.api.raw.pool;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface ConstantMethodTypeInfo extends CpInfo {
    U1 TAG = CONSTANT_METHOD_TYPE;

    @Override
    default U1 getTag() {
        return TAG;
    }

    U2 getDescriptorIndex();

    @Override
    default void setTag(final U1 tag) {
        if (!TAG.equals(tag)) {
            throw new IllegalArgumentException("Invalid tag value for ConstantMethodTypeInfo");
        }
    }

    void setDescriptorIndex(final U2 descriptorIndex);
}
