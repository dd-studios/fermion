package com.dwarveddonuts.fermion.api.raw.frame;

import com.dwarveddonuts.vmtypes.U2;

public interface SameFrame extends StackMapFrame {
    @Override
    default U2 getOffsetDelta() {
        return U2.of(this.getFrameType().intValue());
    }
}
