package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

import java.util.Arrays;
import java.util.List;

public interface Wide extends CodeInstruction {
    U1 OPCODE = WIDE;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getModifiedOpcode() {
        return this.getParameters().get(0);
    }

    default U1 getIndexTop() {
        return this.getParameters().get(1);
    }

    default U1 getIndexBottom() {
        return this.getParameters().get(2);
    }

    default U1 getConstTop() {
        if (!this.getModifiedOpcode().equals(IINC)) {
            throw new UnsupportedOperationException("Not available when widening " + this.getModifiedOpcode() + "'");
        }
        return this.getParameters().get(3);
    }

    default U1 getConstBottom() {
        if (!this.getModifiedOpcode().equals(IINC)) {
            throw new UnsupportedOperationException("Not available when widening " + this.getModifiedOpcode() + "'");
        }
        return this.getParameters().get(4);
    }

    default short getIndex() {
        return (short) (((int) this.getIndexTop().byteValue() << 8) | this.getIndexBottom().intValue());
    }

    default short getConst() {
        return (short) (((int) this.getConstTop().byteValue() << 8) | this.getConstBottom().intValue());
    }

    @Override
    default byte getLength() {
        return (byte) (this.getModifiedOpcode().equals(IINC)? 5 : 3);
    }

    default void setModifiedOpcode(final U1 value) {
        final List<U1> validOps = Arrays.asList(ILOAD, FLOAD, ALOAD, LLOAD, DLOAD, ISTORE, FSTORE,
                ASTORE, LSTORE, DSTORE, RET, IINC);
        if (!validOps.contains(value)) {
            throw new IllegalArgumentException("Unable to widen opcode " + value);
        }
        this.getParameters().set(0, value);
    }

    default void setIndexTop(final U1 value) {
        this.getParameters().set(1, value);
    }

    default void setIndexBottom(final U1 value) {
        this.getParameters().set(2, value);
    }

    default void setConstTop(final U1 value) {
        if (!this.getModifiedOpcode().equals(IINC)) {
            throw new UnsupportedOperationException("Not available when widening " + this.getModifiedOpcode() + "'");
        }
        this.getParameters().set(3, value);
    }

    default void setConstBottom(final U1 value) {
        if (!this.getModifiedOpcode().equals(IINC)) {
            throw new UnsupportedOperationException("Not available when widening " + this.getModifiedOpcode() + "'");
        }
        this.getParameters().set(4, value);
    }

    default void setIndex(final short value) {
        this.setIndexTop(U1.of((byte) (value >> 8)));
        this.setIndexBottom(U1.of((byte) (value & 0xFF)));
    }

    default void setIndex(final int value) {
        this.setIndex((short) value);
    }

    default void setConst(final short value) {
        this.setConstTop(U1.of((byte) (value >> 8)));
        this.setConstBottom(U1.of((byte) (value & 0xFF)));
    }

    default void setConst(final int value) {
        this.setConst((short) value);
    }
}
