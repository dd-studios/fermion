package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U1;

public interface TypeParameterBoundTargetTypeAnnotation extends TypeAnnotation {
    U1 getTypeParameterIndex();
    U1 getBoundIndex();

    void setTypeParameterIndex(final U1 typeParameterIndex);
    void setBoundIndex(final U1 boundIndex);
}
