package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;

public interface TypeArgumentTargetTypeAnnotation extends TypeAnnotation {
    U2 getOffset();
    U1 getTypeArgumentIndex();

    void setOffset(final U2 offset);
    void setTypeArgumentIndex(final U1 typeArgumentIndex);
}
