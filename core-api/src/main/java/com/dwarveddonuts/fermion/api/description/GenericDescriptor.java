package com.dwarveddonuts.fermion.api.description;

public final class GenericDescriptor {
    public static final class Builder {

    }

    public static final GenericDescriptor WILDCARD = new GenericDescriptor("*");

    private final String bytecodeData;

    private GenericDescriptor(final String bytecodeData) {
        this.bytecodeData = bytecodeData;
    }

    @Override
    public String toString() {
        return this.bytecodeData;
    }
}
