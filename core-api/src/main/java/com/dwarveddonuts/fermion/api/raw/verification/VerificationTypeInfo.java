package com.dwarveddonuts.fermion.api.raw.verification;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.U1;

public interface VerificationTypeInfo extends ClassFileSpecPart {
    U1 ITEM_TOP = U1.of(0);
    U1 ITEM_INTEGER = U1.of(1);
    U1 ITEM_FLOAT = U1.of(2);
    U1 ITEM_NULL = U1.of(5);
    U1 ITEM_UNINITIALIZED_THIS = U1.of(6);
    U1 ITEM_OBJECT = U1.of(7);
    U1 ITEM_UNINITIALIZED = U1.of(8);
    U1 ITEM_LONG = U1.of(4);
    U1 ITEM_DOUBLE = U1.of(3);

    U1 getTag();

    void setTag(final U1 tag);
}
