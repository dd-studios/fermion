package com.dwarveddonuts.fermion.api;

import com.dwarveddonuts.fermion.api.description.ClassDescriptor;
import com.dwarveddonuts.fermion.api.description.prototype.ClassPrototype;
import com.dwarveddonuts.fermion.api.flow.ClassResult;
import com.dwarveddonuts.fermion.api.flow.generation.ClassGenerationFlow;
import com.dwarveddonuts.fermion.api.flow.generation.GenerationFlow;
import com.dwarveddonuts.fermion.api.raw.util.RawApi;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface EntryPoint {
    void generate(final Consumer<GenerationFlow> flow);

    RawApi raw();

    default ClassResult generateClass(final ClassPrototype prototype,
                                      final BiConsumer<ClassDescriptor, ClassGenerationFlow> flow) {
        final ClassResult[] wrapper = new ClassResult[1];
        this.generate(it -> wrapper[0] = it.generateClass(prototype, flow));
        return wrapper[0];
    }

    default ClassResult generateSimpleClass(final ClassDescriptor descriptor,
                                            final BiConsumer<ClassDescriptor, ClassGenerationFlow> flow) {
        final ClassResult[] wrapper = new ClassResult[1];
        this.generate(it -> wrapper[0] = it.generateSimpleClass(descriptor, flow));
        return wrapper[0];
    }

    default void raw(final Consumer<RawApi> rawApiConsumer) {
        rawApiConsumer.accept(this.raw());
    }
}
