package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.util.ClassFileSpecPart;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;
import com.dwarveddonuts.vmtypes.U2;
import com.dwarveddonuts.vmtypes.U4;

public interface AttributeInfo extends ClassFileSpecPart {
    U2 getAttributeNameIndex();
    U4 getAttributeLength();
    Array<U1> getInfo();

    void setAttributeNameIndex(final U2 attributeNameIndex);
    void setAttributeLength(final U4 attributeLength);
    void setInfo(final Array<U1> info);
}
