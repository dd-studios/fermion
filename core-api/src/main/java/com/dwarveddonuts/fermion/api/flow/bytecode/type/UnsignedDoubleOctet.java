package com.dwarveddonuts.fermion.api.flow.bytecode.type;

import com.dwarveddonuts.vmtypes.U2;
import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class UnsignedDoubleOctet extends Number implements Comparable<UnsignedDoubleOctet> {
    private static final class Cache {
        private static final short CACHE_MAX = 0xFF;
        private static final int CACHE_SIZE = CACHE_MAX + 1;

        private static final UnsignedDoubleOctet MIN = new UnsignedDoubleOctet(U2.of(MIN_UNSIGNED_DOUBLE_OCTET));
        private static final UnsignedDoubleOctet MAX = new UnsignedDoubleOctet(U2.of(MAX_UNSIGNED_DOUBLE_OCTET));
        private static final UnsignedDoubleOctet[] CACHE = new UnsignedDoubleOctet[CACHE_SIZE];

        static {
            IntStream.range(0, CACHE_SIZE).forEach(it -> CACHE[it] = new UnsignedDoubleOctet(U2.of(it)));
        }
    }

    @Native public static final int MAX_UNSIGNED_DOUBLE_OCTET = U2.MAX_U2;
    @Native public static final int MIN_UNSIGNED_DOUBLE_OCTET = U2.MIN_U2;

    public static final int SIZE = U2.SIZE;
    public static final int BYTES = U2.BYTES;

    private final U2 value;

    private UnsignedDoubleOctet(final U2 value) {
        this.value = value;
    }

    public static UnsignedDoubleOctet of(final U2 value) {
        if (value.intValue() == U2.MIN_U2) return Cache.MIN;
        if (value.intValue() == U2.MAX_U2) return Cache.MAX;
        if (value.intValue() > Cache.CACHE_MAX) return new UnsignedDoubleOctet(value);
        return Cache.CACHE[value.intValue() - MIN_UNSIGNED_DOUBLE_OCTET];
    }

    public static UnsignedDoubleOctet of(final short value) {
        return of(U2.of(value));
    }

    public static UnsignedDoubleOctet of(final int value) {
        if (value < MIN_UNSIGNED_DOUBLE_OCTET || value > MAX_UNSIGNED_DOUBLE_OCTET) {
            throw new IllegalArgumentException(value + "is outside bounds for unsigned double octet: [" +
                    MIN_UNSIGNED_DOUBLE_OCTET + ", " + MAX_UNSIGNED_DOUBLE_OCTET + "]");
        }
        return of((short) value);
    }

    public UnsignedDoubleOctet add(final UnsignedDoubleOctet other) {
        return of(this.value.add(other.value));
    }

    public UnsignedDoubleOctet subtract(final UnsignedDoubleOctet other) {
        return of(this.value.subtract(other.value));
    }

    public UnsignedDoubleOctet multiply(final UnsignedDoubleOctet other) {
        return of(this.value.multiply(other.value));
    }

    public UnsignedDoubleOctet divide(final UnsignedDoubleOctet other) {
        return of(this.value.divide(other.value));
    }

    @Override
    public int compareTo(@NotNull final UnsignedDoubleOctet that) {
        return this.value.compareTo(that.value);
    }

    @Override
    public byte byteValue() {
        return this.value.byteValue();
    }

    @Override
    public short shortValue() {
        return this.value.shortValue();
    }

    @Override
    public int intValue() {
        return this.value.intValue();
    }

    @Override
    public long longValue() {
        return this.value.longValue();
    }

    @Override
    public float floatValue() {
        return this.value.floatValue();
    }

    @Override
    public double doubleValue() {
        return this.value.doubleValue();
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj ||
                (obj instanceof UnsignedDoubleOctet && this.value.equals(((UnsignedDoubleOctet) obj).value));
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
