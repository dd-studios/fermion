package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface IfACmpNe extends CodeInstruction {
    U1 OPCODE = IF_ACMPNE;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getBranchTop() {
        return this.getParameters().get(0);
    }

    default U1 getBranchBottom() {
        return this.getParameters().get(1);
    }

    default short getBranch() {
        return (short) (((int) this.getBranchTop().byteValue() << 8) | this.getBranchBottom().intValue());
    }

    @Override
    default byte getLength() {
        return 2;
    }

    default void setBranchTop(final U1 value) {
        this.getParameters().set(0, value);
    }

    default void setBranchBottom(final U1 value) {
        this.getParameters().set(1, value);
    }

    default void setBranch(final short value) {
        this.getParameters().set(0, U1.of((byte) (value >> 8)));
        this.getParameters().set(1, U1.of((byte) (value & 0xFF)));
    }

    default void setBranch(final int value) {
        this.setBranch((short) value);
    }
}
