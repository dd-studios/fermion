package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface ModulePackagesAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "ModulePackages";

    U2 getPackageCount();
    Array<U2> getPackageIndex();

    void setPackageCount(final U2 packageCount);
    void setPackageIndex(final Array<U2> packageIndex);
}
