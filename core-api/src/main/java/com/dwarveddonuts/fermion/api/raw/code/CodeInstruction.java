package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U1;

import java.util.Arrays;

@SuppressWarnings("SpellCheckingInspection")
public interface CodeInstruction {
    U1 NOP = U1.of(0x00);
    U1 ACONST_NULL = U1.of(0x01);
    U1 ICONST_M1 = U1.of(0x02);
    U1 ICONST_0 = U1.of(0x03);
    U1 ICONST_1 = U1.of(0x04);
    U1 ICONST_2 = U1.of(0x05);
    U1 ICONST_3 = U1.of(0x06);
    U1 ICONST_4 = U1.of(0x07);
    U1 ICONST_5 = U1.of(0x08);
    U1 LCONST_0 = U1.of(0x09);
    U1 LCONST_1 = U1.of(0x0A);
    U1 FCONST_0 = U1.of(0x0B);
    U1 FCONST_1 = U1.of(0x0C);
    U1 FCONST_2 = U1.of(0x0D);
    U1 DCONST_0 = U1.of(0x0E);
    U1 DCONST_1 = U1.of(0x0F);
    U1 BIPUSH = U1.of(0x10);
    U1 SIPUSH = U1.of(0x11);
    U1 LDC = U1.of(0x12);
    U1 LDC_W = U1.of(0x13);
    U1 LDC2_W = U1.of(0x14);
    U1 ILOAD = U1.of(0x15);
    U1 LLOAD = U1.of(0x16);
    U1 FLOAD = U1.of(0x17);
    U1 DLOAD = U1.of(0x18);
    U1 ALOAD = U1.of(0x19);
    U1 ILOAD_0 = U1.of(0x1A);
    U1 ILOAD_1 = U1.of(0x1B);
    U1 ILOAD_2 = U1.of(0x1C);
    U1 ILOAD_3 = U1.of(0x1D);
    U1 LLOAD_0 = U1.of(0x1E);
    U1 LLOAD_1 = U1.of(0x1F);
    U1 LLOAD_2 = U1.of(0x20);
    U1 LLOAD_3 = U1.of(0x21);
    U1 FLOAD_0 = U1.of(0x22);
    U1 FLOAD_1 = U1.of(0x23);
    U1 FLOAD_2 = U1.of(0x24);
    U1 FLOAD_3 = U1.of(0x25);
    U1 DLOAD_0 = U1.of(0x26);
    U1 DLOAD_1 = U1.of(0x27);
    U1 DLOAD_2 = U1.of(0x28);
    U1 DLOAD_3 = U1.of(0x29);
    U1 ALOAD_0 = U1.of(0x2A);
    U1 ALOAD_1 = U1.of(0x2B);
    U1 ALOAD_2 = U1.of(0x2C);
    U1 ALOAD_3 = U1.of(0x2D);
    U1 IALOAD = U1.of(0x2E);
    U1 LALOAD = U1.of(0x2F);
    U1 FALOAD = U1.of(0x30);
    U1 DALOAD = U1.of(0x31);
    U1 AALOAD = U1.of(0x32);
    U1 BALOAD = U1.of(0x33);
    U1 CALOAD = U1.of(0x34);
    U1 SALOAD = U1.of(0x35);
    U1 ISTORE = U1.of(0x36);
    U1 LSTORE = U1.of(0x37);
    U1 FSTORE = U1.of(0x38);
    U1 DSTORE = U1.of(0x39);
    U1 ASTORE = U1.of(0x3A);
    U1 ISTORE_0 = U1.of(0x3B);
    U1 ISTORE_1 = U1.of(0x3C);
    U1 ISTORE_2 = U1.of(0x3D);
    U1 ISTORE_3 = U1.of(0x3E);
    U1 LSTORE_0 = U1.of(0x3F);
    U1 LSTORE_1 = U1.of(0x40);
    U1 LSTORE_2 = U1.of(0x41);
    U1 LSTORE_3 = U1.of(0x42);
    U1 FSTORE_0 = U1.of(0x43);
    U1 FSTORE_1 = U1.of(0x44);
    U1 FSTORE_2 = U1.of(0x45);
    U1 FSTORE_3 = U1.of(0x46);
    U1 DSTORE_0 = U1.of(0x47);
    U1 DSTORE_1 = U1.of(0x48);
    U1 DSTORE_2 = U1.of(0x49);
    U1 DSTORE_3 = U1.of(0x4A);
    U1 ASTORE_0 = U1.of(0x4B);
    U1 ASTORE_1 = U1.of(0x4C);
    U1 ASTORE_2 = U1.of(0x4D);
    U1 ASTORE_3 = U1.of(0x4E);
    U1 IASTORE = U1.of(0x4F);
    U1 LASTORE = U1.of(0x50);
    U1 FASTORE = U1.of(0x51);
    U1 DASTORE = U1.of(0x52);
    U1 AASTORE = U1.of(0x53);
    U1 BASTORE = U1.of(0x54);
    U1 CASTORE = U1.of(0x55);
    U1 SASTORE = U1.of(0x56);
    U1 POP = U1.of(0x57);
    U1 POP2 = U1.of(0x58);
    U1 DUP = U1.of(0x59);
    U1 DUP_X1 = U1.of(0x5A);
    U1 DUP_X2 = U1.of(0x5B);
    U1 DUP2 = U1.of(0x5C);
    U1 DUP2_X1 = U1.of(0x5D);
    U1 DUP2_X2 = U1.of(0x5E);
    U1 SWAP = U1.of(0x5F);
    U1 IADD = U1.of(0x60);
    U1 LADD = U1.of(0x61);
    U1 FADD = U1.of(0x62);
    U1 DADD = U1.of(0x63);
    U1 ISUB = U1.of(0x64);
    U1 LSUB = U1.of(0x65);
    U1 FSUB = U1.of(0x66);
    U1 DSUB = U1.of(0x67);
    U1 IMUL = U1.of(0x68);
    U1 LMUL = U1.of(0x69);
    U1 FMUL = U1.of(0x6A);
    U1 DMUL = U1.of(0x6B);
    U1 IDIV = U1.of(0x6C);
    U1 LDIV = U1.of(0x6D);
    U1 FDIV = U1.of(0x6E);
    U1 DDIV = U1.of(0x6F);
    U1 IREM = U1.of(0x70);
    U1 LREM = U1.of(0x71);
    U1 FREM = U1.of(0x72);
    U1 DREM = U1.of(0x73);
    U1 INEG = U1.of(0x74);
    U1 LNEG = U1.of(0x75);
    U1 FNEG = U1.of(0x76);
    U1 DNEG = U1.of(0x77);
    U1 ISHL = U1.of(0x78);
    U1 LSHL = U1.of(0x79);
    U1 ISHR = U1.of(0x7A);
    U1 LSHR = U1.of(0x7B);
    U1 IUSHR = U1.of(0x7C);
    U1 LUSHR = U1.of(0x7D);
    U1 IAND = U1.of(0x7E);
    U1 LAND = U1.of(0x7F);
    U1 IOR = U1.of(0x80);
    U1 LOR = U1.of(0x81);
    U1 IXOR = U1.of(0x82);
    U1 LXOR = U1.of(0x83);
    U1 IINC = U1.of(0x84);
    U1 I2L = U1.of(0x85);
    U1 I2F = U1.of(0x86);
    U1 I2D = U1.of(0x87);
    U1 L2I = U1.of(0x88);
    U1 L2F = U1.of(0x89);
    U1 L2D = U1.of(0x8A);
    U1 F2I = U1.of(0x8B);
    U1 F2L = U1.of(0x8C);
    U1 F2D = U1.of(0x8D);
    U1 D2I = U1.of(0x8E);
    U1 D2L = U1.of(0x8F);
    U1 D2F = U1.of(0x90);
    U1 I2B = U1.of(0x91);
    U1 I2C = U1.of(0x92);
    U1 I2S = U1.of(0x93);
    U1 LCMP = U1.of(0x94);
    U1 FCMPL = U1.of(0x95);
    U1 FCMPG = U1.of(0x96);
    U1 DCMPL = U1.of(0x97);
    U1 DCMPG = U1.of(0x98);
    U1 IFEQ = U1.of(0x99);
    U1 IFNE = U1.of(0x9A);
    U1 IFLT = U1.of(0x9B);
    U1 IFGE = U1.of(0x9C);
    U1 IFGT = U1.of(0x9D);
    U1 IFLE = U1.of(0x9E);
    U1 IF_ICMPEQ = U1.of(0x9F);
    U1 IF_ICMPNE = U1.of(0xA0);
    U1 IF_ICMPLT = U1.of(0xA1);
    U1 IF_ICMPGE = U1.of(0xA2);
    U1 IF_ICMPGT = U1.of(0xA3);
    U1 IF_ICMPLE = U1.of(0xA4);
    U1 IF_ACMPEQ = U1.of(0xA5);
    U1 IF_ACMPNE = U1.of(0xA6);
    U1 GOTO = U1.of(0xA7);
    U1 JSR = U1.of(0xA8);
    U1 RET = U1.of(0xA9);
    U1 TABLESWITCH = U1.of(0xAA);
    U1 LOOKUPSWITCH = U1.of(0xAB);
    U1 IRETURN = U1.of(0xAC);
    U1 LRETURN = U1.of(0xAD);
    U1 FRETURN = U1.of(0xAE);
    U1 DRETURN = U1.of(0xAF);
    U1 ARETURN = U1.of(0xB0);
    U1 RETURN = U1.of(0xB1);
    U1 GETSTATIC = U1.of(0xB2);
    U1 PUTSTATIC = U1.of(0xB3);
    U1 GETFIELD = U1.of(0xB4);
    U1 PUTFIELD = U1.of(0xB5);
    U1 INVOKEVIRTUAL = U1.of(0xB6);
    U1 INVOKESPECIAL = U1.of(0xB7);
    U1 INVOKESTATIC = U1.of(0xB8);
    U1 INVOKEINTERFACE = U1.of(0xB9);
    U1 INVOKEDYNAMIC = U1.of(0xBA);
    U1 NEW = U1.of(0xBB);
    U1 NEWARRAY = U1.of(0xBC);
    U1 ANEWARRAY = U1.of(0xBD);
    U1 ARRAYLENGTH = U1.of(0xBE);
    U1 ATHROW = U1.of(0xBF);
    U1 CHECKCAST = U1.of(0xC0);
    U1 INSTANCEOF = U1.of(0xC1);
    U1 MONITORENTER = U1.of(0xC2);
    U1 MONITOREXIT = U1.of(0xC3);
    U1 WIDE = U1.of(0xC4);
    U1 MULTIANEWARRAY = U1.of(0xC5);
    U1 IFNULL = U1.of(0xC6);
    U1 IFNONNULL = U1.of(0xC7);
    U1 GOTO_W = U1.of(0xC8);
    U1 JSR_W = U1.of(0xC9);
    U1 BREAKPOINT = U1.of(0xCA);
    U1 IMPDEP1 = U1.of(0xFE);
    U1 IMPDEP2 = U1.of(0xFF);

    static Array<U1> toU1Array(final CodeInstruction... instructions) {
        // Find total length and create final array
        final int totalLength = Arrays.stream(instructions).mapToInt(CodeInstruction::getLength).sum();
        final Array<U1> result = Array.of(totalLength, U1.class);

        // Loop over all instructions and add their bytecode representation to the result array
        // pointer -> start of next instruction
        /*mutable*/ int pointer = 0;
        for (final CodeInstruction instruction : instructions) {
            final Array<U1> serializedInstruction = instruction.toU1Array();
            final byte length = instruction.getLength();

            for (int i = 0; i < length; ++i) {
                result.set(pointer + i, serializedInstruction.get(i));
            }

            pointer += length;
        }

        return result;
    }

    U1 getOpcode();
    Array<U1> getParameters();
    byte getLength();

    default Array<U1> toU1Array() {
        final Array<U1> target = Array.of(this.getLength(), U1.class);
        target.set(0, this.getOpcode());

        final Array<U1> parameters = this.getParameters();
        for (int i = 1, size = this.getLength(); i < size; ++i) {
            target.set(i, parameters.get(i - 1));
        }

        return target;
    }
}
