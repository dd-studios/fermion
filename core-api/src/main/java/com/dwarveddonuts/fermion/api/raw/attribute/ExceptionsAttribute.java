package com.dwarveddonuts.fermion.api.raw.attribute;

import com.dwarveddonuts.fermion.api.raw.attribute.AttributeInfo;
import com.dwarveddonuts.vmtypes.Array;
import com.dwarveddonuts.vmtypes.U2;

public interface ExceptionsAttribute extends AttributeInfo {
    String ATTRIBUTE_NAME = "Exceptions";

    U2 getNumberOfExceptions();
    Array<U2> getExceptionIndexTable();

    void setNumberOfExceptions(final U2 numberOfExceptions);
    void setExceptionIndexTable(final Array<U2> exceptionIndexTable);
}
