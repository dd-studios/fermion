package com.dwarveddonuts.fermion.api.raw.code;

import com.dwarveddonuts.vmtypes.U1;

public interface JsrW extends CodeInstruction {
    U1 OPCODE = JSR_W;

    @Override
    default U1 getOpcode() {
        return OPCODE;
    }

    default U1 getBranchFirst() {
        return this.getParameters().get(0);
    }

    default U1 getBranchSecond() {
        return this.getParameters().get(1);
    }

    default U1 getBranchThird() {
        return this.getParameters().get(2);
    }

    default U1 getBranchFourth() {
        return this.getParameters().get(3);
    }

    default int getBranch() {
        final int first = this.getBranchFirst().byteValue();
        final int second = this.getBranchSecond().intValue();
        final int third = this.getBranchThird().intValue();
        final int fourth = this.getBranchFourth().intValue();
        return (first << 24) | (second << 16) | (third << 8) | fourth;
    }

    @Override
    default byte getLength() {
        return 4;
    }

    default void setBranchFirst(final U1 value) {
        this.getParameters().set(0, value);
    }

    default void setBranchSecond(final U1 value) {
        this.getParameters().set(1, value);
    }

    default void setBranchThird(final U1 value) {
        this.getParameters().set(2, value);
    }

    default void setBranchFourth(final U1 value) {
        this.getParameters().set(3, value);
    }

    default void setBranch(final int value) {
        this.setBranchFirst(U1.of((byte) (value >> 24)));
        this.setBranchSecond(U1.of((byte) ((value >> 16) & 0xFF)));
        this.setBranchThird(U1.of((byte) ((value >> 8) & 0xFF)));
        this.setBranchFourth(U1.of((byte) (value & 0xFF)));
    }
}
