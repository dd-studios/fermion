package com.dwarveddonuts.fermion.api.description.prototype;

import com.dwarveddonuts.fermion.api.description.AccessModifier;
import com.dwarveddonuts.fermion.api.description.MethodDescriptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public final class MethodPrototype {
    public enum Modifier {
        STATIC,
        FINAL,
        SYNCHRONIZED,
        VARARGS,
        NATIVE,
        ABSTRACT,
        STRICT_FP,
        SYNTHETIC,
        BRIDGE
    }

    private final AccessModifier modifier;
    private final MethodDescriptor descriptor;
    private final Set<Modifier> additionalModifiers;

    private MethodPrototype(final AccessModifier modifier, final MethodDescriptor descriptor,
                            final Set<Modifier> additionalModifiers) {
        this.modifier = modifier;
        this.descriptor = descriptor;
        this.additionalModifiers = additionalModifiers;
    }

    public static MethodPrototype simple(final MethodDescriptor descriptor) {
        return simple(AccessModifier.PUBLIC, descriptor);
    }

    public static MethodPrototype simple(final AccessModifier modifier, final MethodDescriptor descriptor) {
        return of(modifier, descriptor); // TODO("Any default modifier?")
    }

    public static MethodPrototype of(final AccessModifier modifier, final MethodDescriptor descriptor,
                                     final Modifier... additionalModifiers) {
        Objects.requireNonNull(modifier, "Modifier cannot be null: use 'DEFAULT' instead");
        Objects.requireNonNull(descriptor, "Unable to create a method with a null descriptor");
        Objects.requireNonNull(additionalModifiers, "Error in vararg: must not be a null array");

        validate(modifier, descriptor, additionalModifiers);

        final var modifiers = EnumSet.noneOf(Modifier.class);
        modifiers.addAll(Arrays.asList(additionalModifiers));
        return new MethodPrototype(modifier, descriptor, modifiers);
    }

    private static void validate(final AccessModifier access, final MethodDescriptor desc, final Modifier... mods) {
        // TODO("")
    }

    public AccessModifier getModifier() {
        return this.modifier;
    }

    public MethodDescriptor getDescriptor() {
        return this.descriptor;
    }

    public Set<Modifier> getAdditionalModifiers() {
        return Collections.unmodifiableSet(this.additionalModifiers);
    }

    @Override
    public String toString() {
        return "MethodPrototype{" +
                "modifier=" + this.modifier +
                ", descriptor=" + this.descriptor +
                ", additionalModifiers=" + this.additionalModifiers +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (MethodPrototype) o;
        return this.modifier == that.modifier && Objects.equals(this.descriptor, that.descriptor) && this.additionalModifiers.equals(that.additionalModifiers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.modifier, this.descriptor, this.additionalModifiers);
    }
}
