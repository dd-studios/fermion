package com.dwarveddonuts.fermion.api.description.prototype;

import com.dwarveddonuts.fermion.api.description.AccessModifier;
import com.dwarveddonuts.fermion.api.description.FieldDescriptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

public final class FieldPrototype {
    public enum Modifier {
        STATIC,
        FINAL,
        VOLATILE,
        TRANSIENT,
        // ENUM, // TODO("Very likely not: these type of fields only exist in enums, so it makes no sense to expose it")
        SYNTHETIC
    }

    private final AccessModifier modifier;
    private final FieldDescriptor field;
    private final Set<Modifier> modifiers;

    private FieldPrototype(final AccessModifier modifier, final FieldDescriptor field, final Set<Modifier> modifiers) {
        this.modifier = modifier;
        this.field = field;
        this.modifiers = modifiers;
    }

    public static FieldPrototype simple(final FieldDescriptor descriptor) {
        return simple(AccessModifier.PUBLIC, descriptor);
    }

    public static FieldPrototype simple(final AccessModifier modifier, final FieldDescriptor descriptor) {
        return of(modifier, descriptor, Modifier.FINAL);
    }

    public static FieldPrototype of(final AccessModifier modifier, final FieldDescriptor descriptor,
                                    final Modifier... additionalModifiers) {
        Objects.requireNonNull(modifier, "Modifier cannot be null: use 'DEFAULT' instead");
        Objects.requireNonNull(descriptor, "Unable to create a field with a null descriptor");
        Objects.requireNonNull(additionalModifiers, "Error in vararg: must not be a null array");

        validate(modifier, descriptor, additionalModifiers);

        final var modifiers = EnumSet.noneOf(Modifier.class);
        modifiers.addAll(Arrays.asList(additionalModifiers));
        return new FieldPrototype(modifier, descriptor, modifiers);
    }

    private static void validate(final AccessModifier access, final FieldDescriptor desc, final Modifier... mods) {
        // TODO("")
    }

    public AccessModifier getAccessModifier() {
        return this.modifier;
    }

    public FieldDescriptor getDescriptor() {
        return this.field;
    }

    public Set<Modifier> getAdditionalModifiers() {
        return Collections.unmodifiableSet(this.modifiers);
    }

    @Override
    public String toString() {
        return "FieldPrototype{" +
                "modifier=" + this.modifier +
                ", field=" + this.field +
                ", additionalModifiers=" + this.modifiers +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final FieldPrototype that = (FieldPrototype) o;
        return this.modifier == that.modifier && Objects.equals(this.field, that.field)
                && Objects.equals(this.modifiers, that.modifiers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.modifier, this.field, this.modifiers);
    }
}
