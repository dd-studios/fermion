package com.dwarveddonuts.fermion.api.raw.annotation.type;

import com.dwarveddonuts.vmtypes.U2;

public interface SupertypeTargetTypeAnnotation extends TypeAnnotation {
    U2 getSupertypeIndex();

    void setSupertypeIndex(final U2 supertypeIndex);
}
