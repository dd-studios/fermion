package com.dwarveddonuts.fermion;

public final class FermionParameters {
    public static final class Builder {
        // HLA API
        // ASM API
        private boolean enableCodeVerifier;
        // Raw API
        private boolean enableRawApiFileFastChecks;
        private boolean enableRawApiFileChecks;

        private Builder() {}

        public Builder enableCodeVerifier() {
            this.enableCodeVerifier = true;
            return this;
        }

        public Builder enableRawApiFileFastChecks() {
            this.enableRawApiFileFastChecks = true;
            return this;
        }

        public Builder enableRawApiFileChecks() {
            this.enableRawApiFileChecks = true;
            return this;
        }

        public FermionParameters make() {
            return new FermionParameters(
                    this.enableRawApiFileFastChecks,
                    this.enableRawApiFileChecks,
                    this.enableCodeVerifier
            );
        }
    }

    private final boolean enableRawApiFileFastChecks;
    private final boolean enableRawApiFileChecks;
    private final boolean enableCodeVerifier;

    private FermionParameters(final boolean enableRawApiFileFastChecks, final boolean enableRawApiFileChecks,
                              final boolean enableCodeVerifier) {
        this.enableRawApiFileFastChecks = enableRawApiFileFastChecks;
        this.enableRawApiFileChecks = enableRawApiFileChecks;
        this.enableCodeVerifier = enableCodeVerifier;
    }

    public static Builder create() {
        return new Builder();
    }

    public boolean enableRawApiFileFastChecks() {
        return this.enableRawApiFileFastChecks;
    }

    public boolean enableRawApiFileChecks() {
        return this.enableRawApiFileChecks;
    }

    public boolean enableCodeVerifier() {
        return this.enableCodeVerifier;
    }

    public void boostrap() {
        Fermion.bootstrap(this);
    }
}
