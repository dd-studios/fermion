plugins {
    id("fermion.java-conventions")
    id("fermion.testing-facilities")
}

dependencies {
    api(project(":vm-types"))
}
