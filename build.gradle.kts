import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.Calendar

plugins {
    `java-library`
    `maven-publish`
    id("net.minecrell.licenser")
    id("com.github.ben-manes.versions")
}

val gitCommit: String
    get() = "git rev-parse --short HEAD".let {
        val process = ProcessBuilder(it).start()
        val input = process.inputStream
        val lines = BufferedReader(InputStreamReader(input)).lines()
        if (process.waitFor() != 0) "no git environment found" else lines.reduce { t, u -> t + u }.get()
    }

val buildNumber: String? get() = System.getProperty("BUILD_NUMBER") ?: System.getProperty("TRAVIS_BUILD_NUMBER")
val buildString get() = buildNumber?.let { "+build.$it" } ?: ""

license {
    header = project.file("NOTICE")
    include("**/*.java")

    ext["year"] = Calendar.getInstance().get(Calendar.YEAR)
    ext["name"] = "Dwarved Donuts Studios"
    ext["email"] = "<EMAIL>"
    ext["app"] = "Fermion"

    ignoreFailures = false
}

val fermionBaseVersion: String by project

group = "com.dwarveddonuts"
version = "$fermionBaseVersion$buildString"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":api"))
    implementation(project(":core-default-implementation"))
}

tasks.withType<Wrapper> {
    gradleVersion = "7.0"
    distributionType = Wrapper.DistributionType.ALL
}
