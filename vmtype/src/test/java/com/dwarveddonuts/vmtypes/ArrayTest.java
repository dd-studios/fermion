package com.dwarveddonuts.vmtypes;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;

public final class ArrayTest {
    private static final class MyType {}
    private record MyFilledType(String data) {}

    @Test
    public void arrayCreationWorks() {
        Assertions.assertThatCode(() -> Array.of(10, MyType.class)).doesNotThrowAnyException();
        Assertions.assertThatCode(() -> Array.of(20)).doesNotThrowAnyException();
        Assertions.assertThatCode(() -> Array.of(0)).doesNotThrowAnyException();
        Assertions.assertThatCode(() -> Array.of(1, MyType.class)).doesNotThrowAnyException();
    }

    @Test
    public void filledArrayCreationWorks() {
        final int size = 3;
        final MyType filling = new MyType();
        final Array<MyType> firstArray = Array.ofFilled(size, filling);
        final Array<MyType> secondArray = Array.ofFilled(size, MyType.class, filling);

        for (int i = 0; i < size; ++i) {
            Assertions.assertThat(firstArray.get(i)).isSameAs(filling);
        }
        Assertions.assertThat(firstArray).isEqualTo(secondArray);
    }

    @Test
    public void invalidLengthOrNullThrowsExceptions() {
        Assertions.assertThatThrownBy(() -> Array.of(10, null)).isInstanceOf(NullPointerException.class);
        Assertions.assertThatThrownBy(() -> Array.of(-1, null)).isInstanceOf(NullPointerException.class);
        Assertions.assertThatThrownBy(() -> Array.of(-1)).isInstanceOf(NegativeArraySizeException.class);
    }

    @Test
    public void noArrayOfArrays() {
        Assertions.assertThatThrownBy(() -> Array.of(1, MyType[].class))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("array type");
    }

    @Test
    public void getAndSetBehaveAsExpected() {
        final Array<MyFilledType> theArray = Array.of(20, MyFilledType.class);

        Assertions.assertThat(theArray.get(6)).isNull();
        Assertions.assertThatCode(() -> theArray.set(6, new MyFilledType("Hello"))).doesNotThrowAnyException();
        Assertions.assertThat(theArray.get(6)).isNotNull()
                .usingComparator(Comparator.comparing(a -> a.data))
                .isEqualTo(new MyFilledType("Hello"));

        theArray.set(0, theArray.get(6));

        Assertions.assertThat(theArray.get(6)).isSameAs(theArray.get(0));

        theArray.set(0, null);

        Assertions.assertThat(theArray.get(0)).isNull();
    }

    @Test
    public void setWrongObjectTypeThrowsException() {
        @SuppressWarnings("unchecked")
        final Array<? super Object> casted = ((Array<? super Object>) (Array<?>) Array.of(1, MyFilledType.class));

        Assertions.assertThatThrownBy(() -> casted.set(0, new MyType())).isInstanceOf(ArrayStoreException.class);
    }

    @Test
    public void setOutsideBoundsThrowsException() {
        Assertions.assertThatThrownBy(() -> Array.of(10).set(10, new Object()))
                .isInstanceOf(ArrayIndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> Array.of(10).set(-1, new Object()))
                .isInstanceOf(ArrayIndexOutOfBoundsException.class);
    }

    @Test
    public void filledArrayIsFilled() {
        final int size = 42;
        final Array<MyType> type = Array.of(size);
        final MyType one = new MyType();
        final MyType two = new MyType();

        Assertions.assertThatCode(() -> type.fill(one)).doesNotThrowAnyException();
        for (int i = 0; i < size; ++i) {
            Assertions.assertThat(type.get(i)).isSameAs(one);
        }

        Assertions.assertThatCode(() -> type.fill(two)).doesNotThrowAnyException();
        for (int i = 0; i < size; ++i) {
            Assertions.assertThat(type.get(i)).isNotSameAs(one);
        }
    }

    @Test
    public void containsAndIndexOf() {
        final Array<MyType> type = Array.of(22);
        final MyType one = new MyType();
        final MyType two = new MyType();
        type.set(20, one);
        type.set(6, one);

        Assertions.assertThat(type.contains(one)).isTrue();
        Assertions.assertThat(type.contains(two)).isFalse();
        Assertions.assertThat(type.indexOf(two)).isEqualTo(-1);
        Assertions.assertThat(type.indexOf(one)).isEqualTo(6);
    }
}
