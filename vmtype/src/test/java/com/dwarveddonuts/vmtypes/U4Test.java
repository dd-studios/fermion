package com.dwarveddonuts.vmtypes;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public final class U4Test {
    @ParameterizedTest
    @ValueSource(ints = {0, (int) U4.MAX_U4, 0xFF, 1, 42})
    public void cacheWorksCorrectly(final int value) {
        final U4 first = U4.of(value);
        final U4 second = U4.of(value);

        Assertions.assertThat(first).isSameAs(second);
        Assertions.assertThat(first.intValue()).isEqualTo(value);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0",
            "559, 559",
            "-1, 4294967295",
            "-12379, 4294954917",
            "1579464, 1579464"
    })
    public void ensureStringIsCorrectlyUnsigned(final int value, final String unsigned) {
        final U4 u4 = U4.of(value);
        Assertions.assertThat(u4.toString()).isEqualTo(unsigned);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0, 0",
            "1025, 934, 1959, 91",
            "1287632129, 954298796, 2241930925, 333333333",
            "-46, -78423, 4294888827, 78377",
            "0, -1, 4294967295, 1"
    })
    public void additionAndSubtractionBehaveCorrectly(final int a, final int b, final long sum, final long diff) {
        final U4 first = U4.of(a);
        final U4 second = U4.of(b);

        Assertions.assertThat(first.add(second)).isEqualByComparingTo(U4.of(sum));
        Assertions.assertThat(first.add(second).longValue()).isEqualTo(sum);
        Assertions.assertThat(first.subtract(second)).isEqualByComparingTo(U4.of(diff));
        Assertions.assertThat(first.subtract(second).longValue()).isEqualTo(diff);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 1, 0, 0",
            "1, 1, 1, 1",
            "484944, 12689, 1858487120, 38",
            "-246464, -897513, 2161311936, 1",
            "-44444567, 546, 1503070194, 7784840" //4250522729
    })
    public void multiplicationAndDivisionBehaveCorrectly(final int a, final int b, final long tim, final long div) {
        final U4 first = U4.of(a);
        final U4 second = U4.of(b);

        Assertions.assertThat(first.multiply(second)).isEqualByComparingTo(U4.of(tim));
        Assertions.assertThat(first.multiply(second).longValue()).isEqualTo(tim);
        Assertions.assertThat(first.divide(second)).isEqualByComparingTo(U4.of(div));
        Assertions.assertThat(first.divide(second).longValue()).isEqualTo(div);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0",
            "42, -58464, -1",
            "42, 84, -1",
            "598, 3, 1",
            "-1, -4564, 1",
            "-2147483648, -2147483648, 0",
            "589, 589, 0",
            "-97, -95, -1"
    })
    public void compare(final int a, final int b, final int result) {
        final U4 first = U4.of(a);
        final U4 second = U4.of(b);

        if (result < 0) {
            Assertions.assertThat(first).usingDefaultComparator().isLessThan(second);
        } else if (result > 0) {
            Assertions.assertThat(first).usingDefaultComparator().isGreaterThan(second);
        } else {
            Assertions.assertThat(first).usingDefaultComparator().isEqualByComparingTo(second);
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 0, Integer.MAX_VALUE, -8658749, -1})
    public void ensureCompareAndEqualsWorkTogether(final int a) {
        Assertions.assertThat(U4.of(a)).isEqualTo(U4.of(a));
        Assertions.assertThat(U4.of(a)).isEqualByComparingTo(U4.of(a));
    }

    @ParameterizedTest
    @ValueSource(longs = {-8L, 4294967296L, -1L, 584789475479457549L})
    public void throwExceptionOnParameterOutsideBounds(final long a) {
        Assertions.assertThatThrownBy(() -> U4.of(a))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("outside bounds");
    }

    @ParameterizedTest
    @ValueSource(strings = {"-8", "4294967296", "-1", "584789475479457549"})
    public void throwExceptionOnStringOutsideBounds(final String a) {
        Assertions.assertThatThrownBy(() -> U4.of(a))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("outside bounds");
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "10", "4127195168"})
    public void createViaString(final String a) {
        Assertions.assertThat(U4.of(a).toString()).isEqualTo(a);
        Assertions.assertThatCode(() -> U4.of(a)).doesNotThrowAnyException();
    }
}
