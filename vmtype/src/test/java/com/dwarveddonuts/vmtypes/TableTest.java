package com.dwarveddonuts.vmtypes;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.Objects;

public class TableTest {
    private interface MyInterface {
        String getData();
    }
    private static final class MyInterfaceImpl implements MyInterface {
        @Override
        public String getData() {
            return null;
        }
    }
    private static final class MySecondInterfaceImpl implements MyInterface {
        private final String data;

        MySecondInterfaceImpl(final String data) {
            this.data = Objects.requireNonNull(data);
        }

        @Override
        public String getData() {
            return this.data;
        }
    }

    @Test
    public void tableCreationWorks() {
        Assertions.assertThatCode(() -> Table.of(10, MyInterface.class)).doesNotThrowAnyException();
        Assertions.assertThatCode(() -> Table.of(1, MyInterface.class)).doesNotThrowAnyException();
    }

    @Test
    public void invalidLengthOrNullThrowsExceptions() {
        Assertions.assertThatThrownBy(() -> Table.of(10, null)).isInstanceOf(NullPointerException.class);
        Assertions.assertThatThrownBy(() -> Table.of(-1, null)).isInstanceOf(NullPointerException.class);
        Assertions.assertThatThrownBy(() -> Table.of(-1, MyInterface.class))
                .isInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void noTableOfArrays() {
        Assertions.assertThatThrownBy(() -> Table.of(1, MyInterface[].class))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("array type");
    }

    @Test
    public void getAndSetBehaveAsExpected() {
        final Table<MyInterface> theTable = Table.of(20, MyInterface.class);

        Assertions.assertThat(theTable.<MyInterface>get(6)).isNull();
        Assertions.assertThat(theTable.get(6, MyInterface.class)).isNull();

        Assertions.assertThatCode(() -> theTable.set(6, new MySecondInterfaceImpl("Hello")))
                .doesNotThrowAnyException();

        Assertions.assertThat(theTable.<MySecondInterfaceImpl>get(6)).isNotNull()
                .usingComparator(Comparator.comparing(MyInterface::getData))
                .isEqualTo(new MySecondInterfaceImpl("Hello"));

        theTable.set(6, new MyInterfaceImpl());
        theTable.set(0, theTable.get(6, MyInterfaceImpl.class));

        Assertions.assertThat(theTable.get(6, MyInterfaceImpl.class)).isSameAs(theTable.get(0));

        theTable.set(0, null);

        Assertions.assertThat(theTable.<MyInterface>get(0)).isNull();
    }

    @Test
    public void getWrongObjectTypeThrowsException() {
        final Table<MyInterface> theTable = Table.of(1, MyInterface.class);
        theTable.set(0, new MyInterfaceImpl());

        //noinspection ResultOfMethodCallIgnored
        Assertions.assertThatThrownBy(() -> theTable.get(0, MySecondInterfaceImpl.class).getData())
                .isInstanceOf(ClassCastException.class);

        @SuppressWarnings("unchecked")
        final Table<? super Object> casted = (Table<? super Object>) (Table<?>) theTable;

        Assertions.assertThatThrownBy(() -> casted.get(0, Object.class))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void setWrongObjectTypeThrowsException() {
        @SuppressWarnings("unchecked")
        final Table<? super Object> casted = (Table<? super Object>) (Table<?>) Table.of(1, MyInterface.class);

        Assertions.assertThatThrownBy(() -> casted.set(0, new Object())).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void getOutsideBoundsThrowsException() {
        Assertions.assertThatThrownBy(() -> Table.of(10, MyInterface.class).get(-8))
                .isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> Table.of(10, MyInterface.class).get(20))
                .isInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void setOutsideBoundsThrowsException() {
        Assertions.assertThatThrownBy(() -> Table.of(10, MyInterface.class).set(10, new MyInterfaceImpl()))
                .isInstanceOf(IndexOutOfBoundsException.class);
        Assertions.assertThatThrownBy(() -> Table.of(10, MyInterface.class).set(-1, new MyInterfaceImpl()))
                .isInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void containsAndIndexOf() {
        final Table<MyInterface> type = Table.of(22, MyInterface.class);
        final MyInterface one = new MyInterfaceImpl();
        final MyInterface two = new MyInterfaceImpl();
        type.set(20, one);
        type.set(6, one);

        Assertions.assertThat(type.contains(one)).isTrue();
        Assertions.assertThat(type.contains(two)).isFalse();
        Assertions.assertThat(type.indexOf(two)).isEqualTo(-1);
        Assertions.assertThat(type.indexOf(one)).isEqualTo(6);
    }
}
