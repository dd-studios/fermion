package com.dwarveddonuts.vmtypes;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public final class U2Test {
    @ParameterizedTest
    @ValueSource(shorts = {0, (short) U2.MAX_U2, 0xFF, 1, 42})
    public void cacheWorksCorrectly(final short value) {
        final U2 first = U2.of(value);
        final U2 second = U2.of(value);

        Assertions.assertThat(first).isSameAs(second);
        Assertions.assertThat(first.shortValue()).isEqualTo(value);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0",
            "15, 15",
            "-1, 65535",
            "-25, 65511",
            "-2564, 62972",
            "18434, 18434"
    })
    public void ensureStringIsCorrectlyUnsigned(final short value, final String unsigned) {
        final U2 u2 = U2.of(value);
        Assertions.assertThat(u2.toString()).isEqualTo(unsigned);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0, 0",
            "84, 42, 126, 42",
            "50, 183, 233, 65403",
            "-4, -8885, 56647, 8881",
            "0, -1, 65535, 1"
    })
    public void additionAndSubtractionBehaveCorrectly(final short a, final short b, final int sum, final int diff) {
        final U2 first = U2.of(a);
        final U2 second = U2.of(b);

        Assertions.assertThat(first.add(second)).isEqualByComparingTo(U2.of(sum));
        Assertions.assertThat(first.add(second).intValue()).isEqualTo(sum);
        Assertions.assertThat(first.subtract(second)).isEqualByComparingTo(U2.of(diff));
        Assertions.assertThat(first.subtract(second).intValue()).isEqualTo(diff);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 1, 0, 0",
            "1, 1, 1, 1",
            "587, 62, 36394, 9",
            "-85, -36, 3060, 0",
            "-2948, 30, 42632, 2086"
    })
    public void multiplicationAndDivisionBehaveCorrectly(final short a, final short b, final int tim, final int div) {
        final U2 first = U2.of(a);
        final U2 second = U2.of(b);

        Assertions.assertThat(first.multiply(second)).isEqualByComparingTo(U2.of(tim));
        Assertions.assertThat(first.multiply(second).intValue()).isEqualTo(tim);
        Assertions.assertThat(first.divide(second)).isEqualByComparingTo(U2.of(div));
        Assertions.assertThat(first.divide(second).intValue()).isEqualTo(div);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0",
            "42, -5468, -1",
            "42, 84, -1",
            "598, 3, 1",
            "-1, -4564, 1",
            "-25535, -25535, 0",
            "589, 589, 0",
            "-97, -95, -1"
    })
    public void compare(final short a, final short b, final int result) {
        final U2 first = U2.of(a);
        final U2 second = U2.of(b);

        if (result < 0) {
            Assertions.assertThat(first).usingDefaultComparator().isLessThan(second);
        } else if (result > 0) {
            Assertions.assertThat(first).usingDefaultComparator().isGreaterThan(second);
        } else {
            Assertions.assertThat(first).usingDefaultComparator().isEqualByComparingTo(second);
        }
    }

    @ParameterizedTest
    @ValueSource(shorts = {1, 0, Short.MAX_VALUE, -86, -1})
    public void ensureCompareAndEqualsWorkTogether(final short a) {
        Assertions.assertThat(U2.of(a)).isEqualTo(U2.of(a));
        Assertions.assertThat(U2.of(a)).isEqualByComparingTo(U2.of(a));
    }

    @ParameterizedTest
    @ValueSource(ints = {-8, 65536, -1, 7841234})
    public void throwExceptionOnParameterOutsideBounds(final int a) {
        Assertions.assertThatThrownBy(() -> U2.of(a))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("outside bounds");
    }

    @ParameterizedTest
    @ValueSource(strings = {"-8", "65536", "-1", "7841234"})
    public void throwExceptionOnStringOutsideBounds(final String a) {
        Assertions.assertThatThrownBy(() -> U2.of(a))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("outside bounds");
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "10", "63474"})
    public void createViaString(final String a) {
        Assertions.assertThat(U2.of(a).toString()).isEqualTo(a);
        Assertions.assertThatCode(() -> U2.of(a)).doesNotThrowAnyException();
    }
}
