package com.dwarveddonuts.vmtypes;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public final class U1Test {
    @ParameterizedTest
    @ValueSource(bytes = {0, Byte.MAX_VALUE, -1, 10, 8, -5})
    public void cacheWorksCorrectly(final byte value) {
        final U1 first = U1.of(value);
        final U1 second = U1.of(value);

        Assertions.assertThat(first).isSameAs(second);
        Assertions.assertThat(first.byteValue()).isEqualTo(value);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0",
            "15, 15",
            "-1, 255",
            "-25, 231"
    })
    public void ensureStringIsCorrectlyUnsigned(final byte value, final String unsigned) {
        final U1 u1 = U1.of(value);
        Assertions.assertThat(u1.toString()).isEqualTo(unsigned);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0, 0",
            "4, 2, 6, 2",
            "0, -1, 255, 1",
            "-8, -3, 245, 251",
            "127, 1, 128, 126"
    })
    public void additionAndSubtractionBehaveCorrectly(final byte a, final byte b, final int sum, final int diff) {
        final U1 first = U1.of(a);
        final U1 second = U1.of(b);

        Assertions.assertThat(first.add(second)).isEqualByComparingTo(U1.of(sum));
        Assertions.assertThat(first.add(second).intValue()).isEqualTo(sum);
        Assertions.assertThat(first.subtract(second)).isEqualByComparingTo(U1.of(diff));
        Assertions.assertThat(first.subtract(second).intValue()).isEqualTo(diff);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 1, 0, 0",
            "1, 1, 1, 1",
            "127, 8, 248, 15",
            "-4, -125, 244, 1",
            "-84, 2, 88, 86"
    })
    public void multiplicationAndDivisionBehaveCorrectly(final byte a, final byte b, final int tim, final int div) {
        final U1 first = U1.of(a);
        final U1 second = U1.of(b);

        Assertions.assertThat(first.multiply(second)).isEqualByComparingTo(U1.of(tim));
        Assertions.assertThat(first.multiply(second).intValue()).isEqualTo(tim);
        Assertions.assertThat(first.divide(second)).isEqualByComparingTo(U1.of(div));
        Assertions.assertThat(first.divide(second).intValue()).isEqualTo(div);
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0, 0",
            "42, 73, -1",
            "42, 20, 1",
            "-30, 127, 1",
            "127, -128, -1",
            "-50, -1, -1",
            "-50, -60, 1",
            "-128, -128, 0",
            "42, 42, 0"
    })
    public void compare(final byte a, final byte b, final int result) {
        final U1 first = U1.of(a);
        final U1 second = U1.of(b);

        if (result < 0) {
            Assertions.assertThat(first).usingDefaultComparator().isLessThan(second);
        } else if (result > 0) {
            Assertions.assertThat(first).usingDefaultComparator().isGreaterThan(second);
        } else {
            Assertions.assertThat(first).usingDefaultComparator().isEqualByComparingTo(second);
        }
    }

    @ParameterizedTest
    @ValueSource(bytes = {1, 0, -1, 127, -123})
    public void ensureCompareAndEqualsWorkTogether(final byte a) {
        Assertions.assertThat(U1.of(a)).isEqualTo(U1.of(a));
        Assertions.assertThat(U1.of(a)).isEqualByComparingTo(U1.of(a));
    }

    @ParameterizedTest
    @ValueSource(ints = {-8, 256, -1, 5843})
    public void throwExceptionOnParameterOutsideBounds(final int a) {
        Assertions.assertThatThrownBy(() -> U1.of(a))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("outside bounds");
    }

    @ParameterizedTest
    @ValueSource(strings = {"-8", "256", "-1", "5843"})
    public void throwExceptionOnStringOutsideBounds(final String a) {
        Assertions.assertThatThrownBy(() -> U1.of(a))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("outside bounds");
    }

    @ParameterizedTest
    @ValueSource(strings = {"0", "10", "210"})
    public void createViaString(final String a) {
        Assertions.assertThat(U1.of(a).toString()).isEqualTo(a);
        Assertions.assertThatCode(() -> U1.of(a)).doesNotThrowAnyException();
    }
}
