package com.dwarveddonuts.vmtypes;

import java.util.Arrays;
import java.util.Objects;

public final class Table<T> {
    private final Class<T> classType;
    private final T[] backend;

    @SuppressWarnings("unchecked")
    private Table(final int size, final Class<T> type) {
        this.classType = type;
        this.backend = (T[]) java.lang.reflect.Array.newInstance(type, size);
    }

    public static <T> Table<T> of(final int size, final Class<T> type) {
        Objects.requireNonNull(type, "Unable to create a table of a null type");
        if (size < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (type.isArray()) {
            throw new IllegalArgumentException("Unable to create a Table for an array type");
        }

        return new Table<>(size, type);
    }

    @SuppressWarnings("unchecked")
    public <U extends T> U get(final int index) {
        return (U) this.get(index, this.classType);
    }

    @SuppressWarnings("unchecked")
    public <U extends T> U get(final int index, final Class<U> type) {
        if (!this.classType.isAssignableFrom(type)) {
            throw new IllegalArgumentException(
                    "Unable to get object of type " + type.getName() + " from " + this.classType.getName()
            );
        }
        try {
            return (U) this.backend[index];
        } catch (final ArrayIndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("Index out of range: " + index);
        }
    }

    public <U extends T> void set(final int index, final U element) {
        if (element != null && !this.classType.isAssignableFrom(element.getClass())) {
            throw new IllegalArgumentException(
                    "Unable to set object of type " + element.getClass().getName() + " for " + this.classType.getName()
            );
        }
        try {
            this.backend[index] = element;
        } catch (final ArrayIndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("Index out of range: " + index);
        }
    }

    public int indexOf(final T element) {
        final var isNull = element == null;
        for (int i = 0; i < this.backend.length; i++) {
            final var atPos = this.backend[i];
            if (isNull) {
                if (atPos == null) return i;
                continue;
            }
            if (element.equals(atPos)) return i;
        }
        return -1;
    }

    public boolean contains(final T element) {
        return this.indexOf(element) != -1;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (Table<?>) o;
        return Arrays.equals(this.backend, that.backend);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.backend);
    }

    @Override
    public String toString() {
        return Arrays.toString(this.backend);
    }
}
