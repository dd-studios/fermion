package com.dwarveddonuts.vmtypes;

import java.util.Arrays;
import java.util.Objects;

public final class Array<T> {
    private final T[] backend;

    @SuppressWarnings("unchecked")
    private Array(final int size, final Class<T> type) {
        this.backend = (T[]) java.lang.reflect.Array.newInstance(type, size);
    }

    public static <T> Array<T> of(final int size, final Class<T> type) {
        Objects.requireNonNull(type, "Unable to create an array of a null type");
        if (size < 0) {
            throw new NegativeArraySizeException();
        }
        if (type.isArray()) {
            throw new IllegalArgumentException("Unable to create an Array for an array type");
        }

        return new Array<>(size, type);
    }

    @SuppressWarnings("unchecked")
    public static <T> Array<T> of(final int size) {
        return (Array<T>) of(size, Object.class);
    }

    public static <T> Array<T> ofFilled(final int size, final Class<T> type, final T filling) {
        final var array = of(size, type);
        array.fill(filling);
        return array;
    }

    public static <T> Array<T> ofFilled(final int size, final T filling) {
        final Array<T> array = of(size);
        array.fill(filling);
        return array;
    }

    public T get(final int index) {
        return this.backend[index];
    }

    public void set(final int index, final T element) {
        this.backend[index] = element;
    }

    public void fill(final T element) {
        Arrays.fill(this.backend, element);
    }

    public int indexOf(final T element) {
        final var isNull = element == null;
        for (var i = 0; i < this.backend.length; i++) {
            final var atPos = this.backend[i];
            if (isNull) {
                if (atPos == null) return i;
                continue;
            }
            if (element.equals(atPos)) return i;
        }
        return -1;
    }

    public boolean contains(final T element) {
        return this.indexOf(element) != -1;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final var that = (Array<?>) o;
        return Arrays.equals(this.backend, that.backend);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(this.backend);
    }

    @Override
    public String toString() {
        return Arrays.toString(this.backend);
    }
}
