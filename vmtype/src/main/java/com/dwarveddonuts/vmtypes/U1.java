package com.dwarveddonuts.vmtypes;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class U1 extends Number implements Comparable<U1> {
    private static final class Cache {
        private static final U1[] CACHE = new U1[MAX_U1 + 1];

        static {
            IntStream.rangeClosed(Byte.MIN_VALUE, Byte.MAX_VALUE)
                    .forEach(it -> CACHE[it - Byte.MIN_VALUE] = new U1((byte) it));
        }
    }

    @Native public static final short MAX_U1 = 0xFF;
    @Native public static final short MIN_U1 = 0x00;

    public static final int SIZE = Byte.SIZE;
    public static final int BYTES = SIZE / Byte.SIZE;

    private static final short BYTE_DISPLACEMENT = MIN_U1 - Byte.MIN_VALUE;

    private final byte value;

    private U1(final byte value) {
        this.value = value;
    }

    public static U1 of(final byte value) {
        return Cache.CACHE[value + BYTE_DISPLACEMENT];
    }

    public static U1 of(final int value) {
        if (value < MIN_U1 || value > MAX_U1) {
            throw new IllegalArgumentException(value + " is outside bounds: [" + MIN_U1 + ", " + MAX_U1 + "]");
        }
        return of((byte) value);
    }

    public static U1 of(final String value) {
        return of(Integer.parseInt(value));
    }

    public U1 add(final U1 other) {
        return of((byte) (this.value + other.value));
    }

    public U1 subtract(final U1 other) {
        return of((byte) (this.value - other.value));
    }

    public U1 multiply(final U1 other) {
        return of((byte) (this.value * other.value));
    }

    public U1 divide(final U1 other) {
        return of((byte) (this.longValue() / other.longValue()));
    }

    @Override
    public int compareTo(@NotNull final U1 that) {
        return Integer.compare(this.intValue(), that.intValue());
    }

    @Override
    public byte byteValue() {
        return this.value;
    }

    @Override
    public short shortValue() {
        return (short) (this.value & MAX_U1);
    }

    @Override
    public int intValue() {
        return this.value & MAX_U1;
    }

    @Override
    public long longValue() {
        return this.value & MAX_U1;
    }

    @Override
    public float floatValue() {
        return this.value & MAX_U1;
    }

    @Override
    public double doubleValue() {
        return this.value & MAX_U1;
    }

    @Override
    public int hashCode() {
        return Byte.hashCode(this.value);
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof U1 && this.value == ((U1) obj).value);
    }

    @Override
    public String toString() {
        return Integer.toUnsignedString(this.intValue());
    }
}
