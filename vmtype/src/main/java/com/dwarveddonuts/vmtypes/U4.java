package com.dwarveddonuts.vmtypes;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class U4 extends Number implements Comparable<U4> {
    private static final class Cache {
        private static final int CACHE_SIZE = 0xFF + 1;

        private static final U4 MIN = new U4((int) MIN_U4);
        private static final U4 MAX = new U4((int) MAX_U4);
        private static final U4[] CACHE = new U4[CACHE_SIZE];

        static {
            IntStream.range(0x00, CACHE_SIZE).forEach(it -> CACHE[it] = new U4(it + 1));
        }
    }

    @Native public static final long MAX_U4 = 0xFFFFFFFFL;
    @Native public static final long MIN_U4 = 0x00000000L;

    public static final int SIZE = Integer.SIZE;
    public static final int BYTES = SIZE / Byte.SIZE;

    private final int value;

    private U4(final int value) {
        this.value = value;
    }

    public static U4 of(final int value) {
        if (value == (int) MIN_U4) return Cache.MIN;
        if (value == (int) MAX_U4) return Cache.MAX;
        if (value < 0) return new U4(value);
        if (value >= Cache.CACHE_SIZE) return new U4(value);
        return Cache.CACHE[value - 1];
    }

    public static U4 of(final long value) {
        if (value < MIN_U4 || value > MAX_U4) {
            throw new IllegalArgumentException(value + " is outside bounds: [" + MIN_U4 + ", " + MAX_U4 + "]");
        }
        return of((int) value);
    }

    public static U4 of(final String value) {
        return of(Long.parseLong(value));
    }

    public U4 add(final U4 other) {
        return of(this.value + other.value);
    }

    public U4 subtract(final U4 other) {
        return of(this.value - other.value);
    }

    public U4 multiply(final U4 other) {
        return of(this.value * other.value);
    }

    public U4 divide(final U4 other) {
        return of((int) (this.longValue() / other.longValue()));
    }

    @Override
    public int compareTo(@NotNull final U4 that) {
        return Long.compare(this.longValue(), that.longValue());
    }

    @Override
    public byte byteValue() {
        return (byte) this.value;
    }

    @Override
    public short shortValue() {
        return (short) this.value;
    }

    @Override
    public int intValue() {
        return this.value;
    }

    @Override
    public long longValue() {
        return this.value & MAX_U4;
    }

    @Override
    public float floatValue() {
        return this.value & MAX_U4;
    }

    @Override
    public double doubleValue() {
        return this.value & MAX_U4;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(this.value);
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof U4 && this.value == ((U4) obj).value);
    }

    @Override
    public String toString() {
        return Integer.toUnsignedString(this.intValue());
    }
}
