package com.dwarveddonuts.vmtypes;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Native;
import java.util.stream.IntStream;

public final class U2 extends Number implements Comparable<U2> {
    private static final class Cache {
        private static final int CACHE_SIZE = 0xFF + 1;

        private static final U2 MIN = new U2((short) MIN_U2);
        private static final U2 MAX = new U2((short) MAX_U2);
        private static final U2[] CACHE = new U2[CACHE_SIZE];

        static {
            IntStream.range(0x00, CACHE_SIZE).forEach(it -> CACHE[it] = new U2((short) (it + 1)));
        }
    }

    @Native public static final int MAX_U2 = 0xFFFF;
    @Native public static final int MIN_U2 = 0x0000;

    public static final int SIZE = Short.SIZE;
    public static final int BYTES = SIZE / Byte.SIZE;

    private final short value;

    private U2(final short value) {
        this.value = value;
    }

    public static U2 of(final short value) {
        if (value == (short) MIN_U2) return Cache.MIN;
        if (value == (short) MAX_U2) return Cache.MAX;
        if (value < (short) 0) return new U2(value);
        if (value >= Cache.CACHE_SIZE) return new U2(value);
        return Cache.CACHE[value - 1];
    }

    public static U2 of(final int value) {
        if (value < MIN_U2 || value > MAX_U2) {
            throw new IllegalArgumentException(value + " is outside bounds: [" + MIN_U2 + ", " + MAX_U2 + "]");
        }
        return of((short) value);
    }

    public static U2 of(final String value) {
        return of(Integer.parseInt(value));
    }

    public U2 add(final U2 other) {
        return of((short) (this.value + other.value));
    }

    public U2 subtract(final U2 other) {
        return of((short) (this.value - other.value));
    }

    public U2 multiply(final U2 other) {
        return of((short) (this.value * other.value));
    }

    public U2 divide(final U2 other) {
        return of((short) (this.longValue() / other.longValue()));
    }

    @Override
    public int compareTo(@NotNull final U2 that) {
        return Integer.compare(this.intValue(), that.intValue());
    }

    @Override
    public byte byteValue() {
        return (byte) this.value;
    }

    @Override
    public short shortValue() {
        return this.value;
    }

    @Override
    public int intValue() {
        return this.value & MAX_U2;
    }

    @Override
    public long longValue() {
        return this.value & MAX_U2;
    }

    @Override
    public float floatValue() {
        return this.value & MAX_U2;
    }

    @Override
    public double doubleValue() {
        return this.value & MAX_U2;
    }

    @Override
    public int hashCode() {
        return Short.hashCode(this.value);
    }

    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof U2 && this.value == ((U2) obj).value);
    }

    @Override
    public String toString() {
        return Integer.toUnsignedString(this.intValue());
    }
}
